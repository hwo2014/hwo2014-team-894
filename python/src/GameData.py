#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple

RaceSession = namedtuple("RaceSession", ["laps", "maxLapTimeMs", "quickRace", "durationMs"])

class GameData(object):

	def __init__(self):
		self.myCarId = None
		self.track = None
		self.carsByName = {}
		self.raceSession = None
		
	def getCars(self):
		return list(self.carsByName.values())

	def addCar(self, car):
		self.carsByName[car.name] = car

	def getCarByName(self, carName):
		try:
			return self.carsByName[carName]
		except KeyError:
			return None

	def getMyCar(self):
		return self.getCarByName(self.myCarId.name)
	
	def isMyCar(self, car):
		return car.id.name == self.myCarId.name

	def getCarsNear(self, car, distance):
		"""
		Restituisce le macchine che
		- distano da car meno di distance
		- sono sulla stessa lane (TODO)
		Cioè le macchine che potenzialmente possono essere urtate.
		"""

		myCar = self.getMyCar()
		near = []
		
		for car in self.carsByName.values():
			if car.name != self.myCarId.name:
				if abs( self.track.getDistance(car.position, myCar.position) ) <= distance or abs( self.track.getDistance(myCar.position, car.position) ) <= distance:
					near.append(car)

		return near

	def getCarsAhead(self, car, distance):
		"""
		Restituisce le macchine che
		- distano da car meno di distance
		- sono sulla stessa lane (TODO: includere chi puo' cambiare lane su uno switch)
		- sono davanti
		Cioè le macchine che potenzialmente possono essere urtate.
		"""

		myCar = self.getMyCar()
		near = []
		
		for car in self.carsByName.values():
			if car.name != self.myCarId.name and car.position.lane.index == myCar.position.lane.index:
				dist = self.track.getDistance(myCar.position, car.position)
				if dist > 0 and dist <= distance:
					near.append(car)

		return near

	def getCarsBehind(self, car, distance):
		"""
		Restituisce le macchine che
		- distano da car meno di distance
		- sono sulla stessa lane (TODO: includere chi puo' cambiare lane su uno switch)
		- sono dietro
		Cioè le macchine che potenzialmente possono essere urtate.
		"""

		myCar = self.getMyCar()
		near = []
		
		for car in self.carsByName.values():
			if car.name != self.myCarId.name and car.position.lane.index == myCar.position.lane.index:
				dist = self.track.getDistance(car.position, myCar.position)
				if dist > 0 and dist <= distance:
					near.append(car)

		return near

	def isQualification(self):
		return self.raceSession.quickRace is None

	def isRace(self):
		return self.raceSession.quickRace is not None and not self.raceSession.quickRace

	def isFreeRace(self):
		return self.raceSession.quickRace is not None and self.raceSession.quickRace