#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
import copy

from Car import *

class Simulator(object):
	
	def __init__(self):
		self.track = None
		self.detective = None
	
	def simulateTick(self, car, action, copyCar = True, crashAngle = None):
		"""
		Simula un singolo tick.
		Non si preoccupa di porre fine al turbo, qualora questo fosse attivo
		(però ne tiene conto per calcolare l'accelerazione).
		"""
		if crashAngle is None:
			crashAngle = self.detective.getCrashAngle()
		
		if copyCar:
			carCopy = copy.copy(car)
			carCopy.SIMULATION_CAR = True
		else:
			carCopy = car

		assert carCopy.SIMULATION_CAR is True

		if isinstance(action, SwitchAction):
			carCopy.pendingSwitch = action.switch
		elif isinstance(action, ThrottleAction):
			carCopy.setThrottle(action.throttle)
		elif isinstance(action, TurboAction):
			carCopy.turboStart(car.tick+1)
		else:
			raise Exception("Unknown action")
		
		if carCopy.tick is not None:
			carCopy.tick += 1

		# Disattivato, magari era causa di bug
		# Tanto la frenata massima non cambia
		#if carCopy.isTurboActive() and carCopy.turboStartTick is not None:
		#	if carCopy.turboStartTick + carCopy.turboAvailable.turboDurationTicks < carCopy.tick:
		#		carCopy.turboEnd()

		carCopy.acc = self.detective.getTanAcc(
			velocity = carCopy.vel,
			throttle = carCopy.throttle,
			turbo = carCopy.getTurboFactor()
		)
		carCopy.angAcc = self.detective.getAngAcc(
			curvature = carCopy.getCurrentCurvature(),
			carAngle = carCopy.angle,
			velocity = carCopy.vel,
			angularVelocity = carCopy.angVel
		)
		
		carCopy.setVel(carCopy.vel + carCopy.acc)
		carCopy.setAngVel(carCopy.angVel + carCopy.angAcc)
		
		carCopy.addDistance(carCopy.vel)
		carCopy.setAngle(carCopy.angle + carCopy.angVel)
		
		thisPiece = self.track.getPiece(carCopy.position.pieceIndex)

		if abs(carCopy.angle) >= crashAngle * thisPiece.getCrashAngleFactor():
			carCopy.setCrashed(True)

		return carCopy
	
	def simulate(self, car, actions, laps=9999999999, crashAngle=None):
		copyCar = True
		carCopy = car
		
		if crashAngle is None:
			crashAngle = self.detective.getCrashAngle()
		
		for action in actions:
			carCopy = self.simulateTick(carCopy, action, copyCar, crashAngle)
			
			if carCopy.position.lap >= laps:
				# La macchina è arrivata al traguardo!
				carCopy.arrived = True
				break
		
			if carCopy.crashed:
				break

			copyCar = False
		
		return carCopy
	
	def simulateConstantThrottle(self, car, throttle, numTicks, laps, crashAngle=None):
		"""
		Avvia una simulazione lunga numTicks, in cui viene sempre usato throttle come throttle.
		e tutti gli altri throttle=0.
		laps = numero di giri
		"""
		actions = [ ThrottleAction(throttle=throttle, tick=None) ] * numTicks
		# print actions
		return self.simulate(car, actions, laps, crashAngle)
	
	def simulateBrake(self, car, firstThrottle, numTicks, laps, crashAngle=None):
		"""
		Avvia una simulazione lunga numTicks+1, in cui viene messo firstThrottle come primo throttle
		e tutti gli altri throttle=0.
		laps = numero di giri
		"""
		actions = [ ThrottleAction(throttle=firstThrottle, tick=None) ] + [ ThrottleAction(throttle=0, tick=None) ] * numTicks
		# print actions
		return self.simulate(car, actions, laps, crashAngle)
	
	def simulateSwitchAndBrake(self, car, switchAction, numTicks, laps, crashAngle=None):
		"""
		Avvia una simulazione lunga numTicks+1, in cui viene messo switchAction come prima azione
		e tutti gli altri throttle=0.
		laps = numero di giri
		"""
		actions = [ switchAction ] + [ ThrottleAction(throttle=0, tick=None) ] * numTicks
		# print actions
		return self.simulate(car, actions, laps, crashAngle)
	
	
