#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from __future__ import division

import json
import sys
import traceback
import math, numpy
import os

from Car import *
from Track import *
from Piece import *
from GameData import *

class GameBot(object):

	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key
		
		self.gameData = None
		self.detective = None
		self.controlTower = None

	def sendMessage(self, msgType, data=None, gameTick=None, gameId=None):
		message = {
			"msgType": msgType
		}
		if data is not None:
			message["data"] = data
		if gameTick is not None:
			message["gameTick"] = gameTick
		if gameId is not None:
			message["gameId"] = gameId

		self.socket.sendall(json.dumps(message) + "\n")

	def handleMessage(self, msgType, data=None, gameTick=None, gameId=None):
		try:
			handler = getattr(self, "on_"+msgType)
		except AttributeError:
			handler = None

		if handler is not None:
			kwargs = {}
			if gameTick is not None:
				kwargs["gameTick"] = gameTick
			if gameId is not None:
				kwargs["gameId"] = gameId
			
			handler(data, **kwargs)
		else:
			print "Unknown message type: {0}. Data: {1}\n".format(msgType, data)
			self.ping()

	def receiveLoop(self):
		socket_file = self.socket.makefile()
		line = socket_file.readline()
		while line:
			try:
				message = json.loads(line)
				
				msgType = message['msgType']
				data = message['data']
				gameTick = None
				gameId = None
				if 'gameTick' in message:
					gameTick = message['gameTick']
				if 'gameId' in message:
					gameId = message['gameId']

				self.handleMessage(msgType, data, gameTick=gameTick, gameId=gameId)
			except Exception as err:
				print "Exception:", err
				print "The message was:", line
				print "Traceback:", traceback.format_exc()
				self.ping()
			finally:
				line = socket_file.readline()

		print "Connection closed."

	def run(self):
		self.join()
		# Loop
		self.receiveLoop()

	#####################
	#  Receive message  #
	#####################
	
	def on_join(self, data):
		print "join ack"

	def on_joinRace(self, data):
		print "joinRace ack"

	def on_createRace(self, data):
		print "createRace ack"

	def on_yourCar(self, data, gameId=None):
		print "yourCar"
		
		self.gameData.myCarId = CarId(
			data["name"],
			data["color"]
		)
		
		#self.ping()

	def on_gameInit(self, data, gameId=None):
		print "gameInit", data

		lanes = [None] * len(data["race"]["track"]["lanes"])
		for l in data["race"]["track"]["lanes"]:
			lanes[l["index"]] = Lane(
				distanceFromCenter = l["distanceFromCenter"],
				index = l["index"]
			)

		absoluteStartAngle = 0.0
		pieces = []
		for (index, p) in enumerate(data["race"]["track"]["pieces"]):
			if "radius" in p:
				pieces.append(BendPiece(
					lanes = lanes,
					absoluteStartAngle = absoluteStartAngle,
					radius = p["radius"],
					bendAngle = p["angle"]/180*math.pi,
					switch = "switch" in p and p["switch"]
				))

				absoluteStartAngle += p["angle"] / 180 * math.pi
			else:
				pieces.append(StraightPiece(
					lanes = lanes,
					absoluteStartAngle = absoluteStartAngle,
					length = p["length"],
					switch = "switch" in p and p["switch"]
				))

		# Non ricreare tutto quando inizia la race
		if self.gameData.track is None:
			self.gameData.track = Track(
				data["race"]["track"]["id"],
				data["race"]["track"]["name"],
				pieces,
				lanes
			)

			# Calcola il rettilineo più lungo
			bestStartingPoint = None
			maxLenght = None
			startingPoint = 0
			while startingPoint < len(pieces):
				
				length = 0
				endPoint = startingPoint
				
				while isinstance(self.gameData.track.getPiece(endPoint), StraightPiece):
					length += self.gameData.track.getPiece(endPoint).length
					endPoint += 1
				
				if not maxLenght or maxLenght < length:
					maxLenght = length
					bestStartingPoint = startingPoint

				startingPoint = endPoint + 1

			self.gameData.track.longestRun = bestStartingPoint
			print "LONGEST RUN: %d" % bestStartingPoint

		self.controlTower.simulator.track = self.gameData.track

		for car in data["race"]["cars"]:
			carId = CarId(
				name = car["id"]["name"],
				color = car["id"]["color"]
			)
			dimensions = Dimensions(
				length = car["dimensions"]["length"],
				width = car["dimensions"]["width"],
				guideFlagPosition = car["dimensions"]["guideFlagPosition"]
			)

			newCar = Car(
				self.gameData,
				carId,
				dimensions,
				self.detective
			)

			# Non ricreare la macchina quando inizia la race
			if self.gameData.getCarByName(car["id"]["name"]) is None:
				self.gameData.addCar(newCar)

			self.gameData.getCarByName(car["id"]["name"]).gameInit()

		if "quickRace" in data["race"]["raceSession"]:
			# Quickrace (quelle di test) o Race (se quickrace è false)
			self.gameData.raceSession = RaceSession(
				laps = data["race"]["raceSession"]["laps"],
				maxLapTimeMs = data["race"]["raceSession"]["maxLapTimeMs"],
				quickRace = data["race"]["raceSession"]["quickRace"],
				durationMs = None
			)
			self.gameData.track.setLaps(data["race"]["raceSession"]["laps"])
		else:
			# Qualification
			self.gameData.raceSession = RaceSession(
				laps = None,
				maxLapTimeMs = None,
				quickRace = None,
				durationMs = data["race"]["raceSession"]["durationMs"]
			)
		
		self.controlTower.gameInit()

		#self.ping()

	def on_carPositions(self, data, gameTick=None, gameId=None):
		#print "carPositions", data, gameTick

		for carData in data:
			lanePosition = LanePosition(
				startLaneIndex = carData["piecePosition"]["lane"]["startLaneIndex"],
				endLaneIndex = carData["piecePosition"]["lane"]["endLaneIndex"]
			)

			position = Position(
				pieceIndex = carData["piecePosition"]["pieceIndex"],
				inPieceDistance = carData["piecePosition"]["inPieceDistance"],
				lane = lanePosition,
				lap = carData["piecePosition"]["lap"]
			)

			car = self.gameData.getCarByName(carData["id"]["name"])
			
			car.updatePosition(
				carData["angle"]/180*math.pi,
				position,
				gameTick
			)

			# Quando la macchina crasha ci dà dati che sono tutti sbagliati
			# Quando ci sono urti i dati sono tutti sbagliati
			if not car.crashed \
				and not car.disqualified \
				and len(self.gameData.getCarsNear(car, 20)) == 0:
				
				# Manda gli indizi al detective
				if self.gameData.isMyCar(car):
					self.detective.seenMyself(car)
				else:
					self.detective.seenSomeoneElse(car)

				self.detective.seenSomebody(car)

		car = self.gameData.getMyCar()
		

		if gameTick is not None and gameTick%41 == 0: print ""

		expectedTanAcc = self.controlTower.simulator.detective.getTanAcc(car.previousVel, car.throttle, car.getTurboFactor())
		tanError = car.acc - expectedTanAcc
		if abs(tanError) > 0.00001:
			if gameTick is not None and gameTick%41 == 0: print "Tick %4d: Expected tangential acceleration: %lf (errore assoluto: %lf)" % (gameTick, expectedTanAcc, tanError)
	
		expectedAngAcc = self.controlTower.simulator.detective.getAngAcc(car.previousCurvature, car.previousAngle, car.previousVel, car.previousAngVel)
		angError = car.angAcc - expectedAngAcc
		if abs(angError) > 0.00001:
			if gameTick is not None and gameTick%41 == 0: print "Tick %4d: Expected angular acceleration: %lf (errore assoluto: %lf)" % (gameTick, expectedAngAcc, angError)
		
		# Se la NOSTRA macchina è appena uscita dallo switch allora forziamo che le velocità e le accelerazioni siano quelle expected e non
		# quelle calcolate in updatePosition.
		# La ragione è che in Japan, ad esempio, uscito dallo switch sul rettilineo updatePosition crede che la macchina vada lenta (ma solo perchè
		# lo switch è più lungo della lunghezza salvata del piece) e per questo noi mettiamo il throttle a 1.0 (disastro)
		if car.previousPosition \
		 and car.previousPosition.lane.startLaneIndex != car.previousPosition.lane.endLaneIndex \
		 and car.previousPosition.pieceIndex != car.position.pieceIndex \
		 and abs(tanError) > 1e-4 \
		 and self.detective.complexAngConstantsFound \
		 and self.detective.simpleAngConstantsFound \
		 and self.detective.tanConstantsFound:

			if gameTick is not None and gameTick%41 == 0: print "WARNING: le grandezze tangenziali sono state forzate dopo lo switch"
			if gameTick is not None and gameTick%41 == 0: print "VECCHI dati:"
			if gameTick is not None and gameTick%41 == 0: car.printStatus()

			car.vel = car.previousVel + expectedTanAcc
			car.acc = expectedTanAcc
			if gameTick is not None and gameTick%41 == 0: print "NUOVI dati:"

		if gameTick is not None and gameTick%41 == 0: car.printStatus()
		
		
		if gameTick != None:
			self.controlTower.checkMode()
			
			action = self.controlTower.getAction(tick = gameTick)
			
			if isinstance(action, ThrottleAction):
				if self.gameData.getMyCar().throttle != action.throttle:
					print "WARNING: in on_carPositions action.throttle è diverso da myCar.throttle"
				self.throttle(action.throttle, action.tick)
			elif isinstance(action, SwitchAction):
				self.switchLane(action.switch, action.tick)
			elif isinstance(action, TurboAction):
				self.turbo(action.message, action.tick)
			else:
				self.ping()

	def on_gameStart(self, data, gameTick=None, gameId=None):
		print "gameStart"
		
		if gameTick != None:
			self.controlTower.checkMode()
			
			action = self.controlTower.getAction(tick = gameTick)
			
			if isinstance(action, ThrottleAction):
				if self.gameData.getMyCar().throttle != action.throttle:
					print "WARNING: in on_carPositions action.throttle è diverso da myCar.throttle"
				self.throttle(action.throttle, action.tick)
			elif isinstance(action, SwitchAction):
				self.switchLane(action.switch, action.tick)
			elif isinstance(action, TurboAction):
				self.turbo(action.message, action.tick)
			else:
				self.ping()

	def on_gameEnd(self, data, gameId=None):
		print "gameEnd", data

		# TODO

		self.ping()

	def on_tournamentEnd(self, data, gameId=None):
		print "tournamentEnd"
		self.ping()

	def on_crash(self, data, gameTick=None, gameId=None):
		print "crash", data, gameTick

		car = self.gameData.getCarByName(data["name"])
		car.setCrashed(True, gameTick);

		#self.ping()

	def on_spawn(self, data, gameTick=None, gameId=None):
		print "spawn", data, gameTick

		car = self.gameData.getCarByName(data["name"])
		car.setCrashed(False, gameTick);

		#self.ping()

	def on_lapFinished(self, data, gameTick=None, gameId=None):
		print "lapFinished", data, gameTick

		# TODO

		#self.ping()

	def on_dnf(self, data, gameTick=None, gameId=None):
		print "dnf", data, gameTick

		car = self.gameData.getCarByName(data["car"]["name"])
		car.setDisqualified(data["reason"], gameTick);

		#self.ping()

	def on_finish(self, data, gameTick=None, gameId=None):
		print "finish", data, gameTick
		self.ping()

	def on_turboAvailable(self, data, gameTick=None, gameId=None):
		print "turboAvailable", data, gameTick
		
		turbo = Turbo(
			turboDurationMilliseconds = data["turboDurationMilliseconds"],
			turboDurationTicks = data["turboDurationTicks"],
			turboFactor = data["turboFactor"]
		)

		for car in self.gameData.getCars():
			car.setTurboAvailable(turbo)
		
		#self.ping()

	def on_turboStart(self, data, gameTick=None, gameId=None):
		print "turboStart", data, gameTick
		
		car = self.gameData.getCarByName(data["name"])
		car.turboStart(gameTick)
		
		#self.ping()

	def on_turboEnd(self, data, gameTick=None, gameId=None):
		print "turboEnd", data, gameTick
		
		car = self.gameData.getCarByName(data["name"])
		car.turboEnd(gameTick)
		
		#self.ping()

	##################
	#  Send message  #
	##################
	
	def turbo(self, message, gameTick):
		print "Tick {0}: turbo {1}".format(gameTick, message)
		self.sendMessage("turbo", message, gameTick=gameTick)

	def throttle(self, throttle, gameTick):
		#print "Tick {0}: throttle {1}".format(gameTick, throttle)
		self.sendMessage("throttle", throttle, gameTick=gameTick)

	def switchLane(self, direction, gameTick):
		print "Tick {0}: switchLane {1}".format(gameTick, direction)
		
		if direction in ["Left", "Right"]:
			self.sendMessage("switchLane", direction, gameTick=gameTick)
		else:
			print "/!\\Error: switchLane not valid: ", direction
			self.ping()

	def join(self):
		print "join"
		data = {
			"name": self.name,
			"key": self.key
		}
		self.sendMessage("join", data)
	
	def createRace(self, carCount=3, trackName=None, password=None):
		print "createRace", carCount, trackName, password
		data = {
			"botId": {
				"name": self.name,
				"key": self.key
			},
			"carCount": int(carCount)
		}
		if password is not None:
			data["password"] = password
		if trackName is not None:
			data["trackName"] = trackName
		
		self.sendMessage("createRace", data)

	def joinRace(self, carCount=None, trackName=None, password=None):
		print "joinRace", carCount, trackName, password
		data = {
			"botId": {
				"name": self.name,
				"key": self.key
			}
		}
		if carCount is not None:
			data["carCount"] = int(carCount)
		if password is not None:
			data["password"] = password
		if trackName is not None:
			data["trackName"] = trackName

		self.sendMessage("joinRace", data)
	
	def ping(self):
		print "ping"
		self.sendMessage("ping")
