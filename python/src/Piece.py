#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
import bisect

class BasePiece(object):
	
	def getCurvature(self, startLane, endLane=None, inPieceDistance=0):
		if endLane is None:
			endLane = startLane
		
		if startLane == endLane:
			# Non stiamo switchando
			return self.getBasicCurvature(startLane, endLane, inPieceDistance)
		
		else:
			curvatures = self.getCurvaturesList(startLane, endLane)
			
			if len(curvatures) > 0:
				# Abbiamo dati da utilizzare!
				index = bisect.bisect_left( curvatures, (inPieceDistance, 0) )
				l = len(curvatures)
				if index == 0:
					# inPieceDistance è minore di tutti i dati che abbiamo
					c = curvatures[0][1]
				elif index == l:
					# inPieceDistance è maggiore di tutti i dati che abbiamo
					c = curvatures[l-1][1]
				else:
					# Siamo a metà tra due dati
					try:
						x = curvatures[index-1]
						y = curvatures[index]
						c = ( (inPieceDistance - x[0]) * y[1] + (y[0] - inPieceDistance) * x[1] ) / (y[0] - x[0])
					except Exception:
						c = curvatures[index][1]
				
				return c
			
			else:
				# Non abbiamo dati, purtroppo
				return self.getDefaultCurvature(startLane, endLane, inPieceDistance)
	
	def __init__(self, avgLength = 100.0):
		self.crashAngleFactor = 1.0
		self.crashCounter = 0
		self.avgLength = avgLength

	def lowerCrashAngleFactor(self, rescale = 0.95):
		print "WARNING: CrashAngle del pezzo %s abbassato a %lf" % (self, self.crashAngleFactor * rescale)
		self.crashAngleFactor *= rescale

	def getCrashAngleFactor(self):
		return self.crashAngleFactor

class StraightPiece(BasePiece):
	def __init__(self, lanes, absoluteStartAngle, length, switch=False):
		BasePiece.__init__(self, avgLength = length)

		self.lanes = lanes
		self.absoluteStartAngle = absoluteStartAngle
		self.length = length
		self.switch = switch
		self.switchLength = length
		self.switchCurvature = []

	def getLength(self, startLane, endLane=None):
		if endLane == None: endLane = startLane
		
		if startLane != endLane:
			return self.switchLength

		return self.length
	
	def getBasicCurvature(self, startLane, endLane=None, inPieceDistance=0):
		"""
		Ritorna la curvatura ipotizzando assenza di switch
		"""
		return 0
	
	def getDefaultCurvature(self, startLane, endLane=None, inPieceDistance=0):
		"""
		Ritorna la curvatura sapendo di non conoscere lo switch
		"""
		if endLane == None: endLane = startLane

		# Se non cambiamo lane
		if endLane == startLane:
			return 0

		# TODO
		return 0

	def getCurvaturesList(self, startLane, endLane):
		"""
		Ritorna la lista con le curvature memorizzate
		"""
		return self.switchCurvature

	def __repr__(self):
		return "STRAIGHT PIECE\tlength: %ld, lanes: %s, switchLength: %s, crashAngleFactor: %lf" % (self.length, self.lanes, self.switchLength, self.crashAngleFactor)


class BendPiece(BasePiece):
	def __init__(self, lanes, absoluteStartAngle, radius, bendAngle, switch=False):
		BasePiece.__init__(self, avgLength = abs(radius * bendAngle))

		self.lanes = lanes
		self.absoluteStartAngle = absoluteStartAngle
		self.radius = radius
		self.bendAngle = bendAngle
		self.switch = switch
		self.switchLength = {}
		self.switchCurvature = {}

		# TODO: ci piace?
		# if self.switch:
		# 	self.lowerCrashAngleFactor(0.975)

	def getAbsoluteRadius(self, startLane, endLane=None, inPieceDistance=0):
		"""
		Restituisce il raggio (valore assoluto) della curva alla corsia lane
		Solleva una eccezione se pieceIndex non è una curva
		"""
		if endLane == None: endLane = startLane
		
		if startLane == endLane:
			# Non è uno switch
			if self.bendAngle > 0:
				return self.radius - self.lanes[startLane].distanceFromCenter
			else:
				return self.radius + self.lanes[startLane].distanceFromCenter
		else:
			# È uno switch
			# Restituisci il minimo dei due estremi dell'intervallo dei possibili valori?
			return min(self.getAbsoluteRadius(startLane), self.getAbsoluteRadius(endLane))

	def getSignedRadius(self, startLane, endLane=None, inPieceDistance=0):
		"""
		Restituisce il raggio (con segno) della curva pieceIndex alla corsia lane
		Solleva una eccezione se pieceIndex non è una curva
		
		positivo = gira verso destra
		negativo = gira verso sinistra
		"""
		if endLane == None: endLane = startLane
		
		absRadius = self.getAbsoluteRadius(startLane, endLane, inPieceDistance)
		if self.bendAngle > 0:
			return absRadius
		else:
			return -absRadius

	def getBendAngle(self):
		return self.bendAngle
	
	def getLength(self, startLane, endLane=None):
		if endLane == None: endLane = startLane
		
		if startLane != endLane and (startLane, endLane) in self.switchLength.keys():
			return self.switchLength[(startLane, endLane)]
		elif startLane != endLane and (endLane, startLane) in self.switchLength.keys():
			return self.switchLength[(endLane, startLane)]
		else:
			return self.getAbsoluteRadius(startLane) * abs(self.bendAngle)

	def getBasicCurvature(self, startLane, endLane=None, inPieceDistance=0):
		"""
		Ritorna la curvatura ipotizzando assenza di switch.
		Per retrocompatibilità (e anche per pigrizia mia), chiede anche endLane e inPieceDistance (ma non servirebbero)
		"""
		if endLane == None: endLane = startLane
		
		return 1/self.getSignedRadius(startLane, endLane, inPieceDistance)

	def getDefaultCurvature(self, startLane, endLane=None, inPieceDistance=0):
		"""
		Ritorna la curvatura sapendo di non conoscere lo switch
		"""
		if endLane == None: endLane = startLane

		# Non abbiamo dati, purtroppo
		# 0: assumi che la getBasicCurvature sia esatta
		# >0: prendi la peggiore curvatura ed esagera la differenza
		#     tra la curvatura delle due lane
		parameter = 1.5
		c1 = self.getBasicCurvature(startLane, startLane, inPieceDistance)
		c2 = self.getBasicCurvature(endLane, endLane, inPieceDistance)
		if abs(c1) > abs(c2):
			return c1 + (c1 - c2) * parameter
		else:
			return c2 + (c2 - c1) * parameter

	def getCurvaturesList(self, startLane, endLane):
		"""
		Ritorna la lista con le curvature memorizzate
		"""
		lanes = (startLane, endLane)
		
		if not lanes in self.switchCurvature.keys():
			self.switchCurvature[lanes] = []
		
		return self.switchCurvature[lanes]

	def __repr__(self):
		return "BEND PIECE\tcentral length:%lf, radius: %lf, bendAngle: %lf, lanes: %s, switchLenght: %s, crashAngleFactor: %lf" % (abs(self.bendAngle * self.radius), self.radius, self.bendAngle, self.lanes, self.switchLength, self.crashAngleFactor)

