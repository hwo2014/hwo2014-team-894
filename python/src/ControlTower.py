#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple
import random
import operator
import numpy
import math
import copy

from Car import *
from Piece import *

class ControlTower(object):
	
	# Possibili modalità / tipi di gara
	DEDUCTION = 'Deduction'			# Modalità di deduzione delle costanti
	SLALOM = 'Slalom'			    # Modalità di esplorazione degli switch
	QUALIFICATION = 'Qualification'	# Qualifiche --> cerca di fare il giro più veloce
	RACE = 'Race'					# Gara vera e propria --> cerca di arrivare per primo alla fine
	
	def gameInit(self):
		"""
		Inizializza la modalità (viene chiamata da GameBot quando viene ricevuto l'evento gameInit)
		"""
		if self.gameData.isQualification():
			self.mode = self.DEDUCTION
		elif self.gameData.isFreeRace():
			self.mode = self.DEDUCTION
		else:
			self.mode = self.RACE
		
		print "Initialized ControlTower Mode: %s" % (self.mode, )
	
	def checkMode(self):
		"""
		Dovrebbe essere chiamata periodicamente.
		Aggiorna la modalità di gara, se necessario
		"""
		
		if self.mode == self.DEDUCTION:
			# Controllo se le costanti fisiche sono state trovate
			constantsFound = self.detective.tanConstantsFound \
				and self.detective.simpleAngConstantsFound \
				and self.detective.complexAngConstantsFound
			if constantsFound or self.gameData.getMyCar().position.lap > 1:
				# Passo alla modalità giusta
				if self.gameData.isQualification():
					self.mode = self.SLALOM
				elif self.gameData.isFreeRace():
					self.mode = self.SLALOM
				else:
					# Questo in teoria non dovrebbe accadere
					self.mode = self.RACE
				print "ControlTower Mode changed: %s" % (self.mode,)
		
		if self.mode == self.SLALOM:
			# Smettiamo a due giri dall'inizio
			if self.gameData.getMyCar().position.lap > 1:
				# Passo alla modalità giusta
				if self.gameData.isQualification():
					self.mode = self.QUALIFICATION
				elif self.gameData.isFreeRace():
					self.mode = self.RACE
				else:
					# Questo in teoria non dovrebbe accadere
					self.mode = self.RACE
				print "ControlTower Mode changed: %s" % (self.mode,)
		
	def __init__(self):
		self.gameData = None
		self.simulator = None
		self.detective = None
		
		# Modalità corrente (DEDUCTION / QUALIFICATION / RACE)
		self.mode = None
	
	
	def getSwitchForOptimalPath(self, position):
		"""
		Restituisce lo switch ("Right" | "Left" | None) che serve a percorrere il cammino di lunghezza minima.
		"""
		
		track = self.gameData.track

		switchIndex = track.getNextSwitchPieceIndex(position.pieceIndex)

		bestSwitch = None
		bestLength = track.minimumDistanceToEnd(Position(
			pieceIndex = switchIndex,
			inPieceDistance = 0,
			lap = position.lap,
			lane = LanePosition(
				startLaneIndex = position.lane.endLaneIndex,
				endLaneIndex = position.lane.endLaneIndex
			)
		))
		#print "currLength", switchIndex, bestLength

		if self.mode == self.SLALOM:
			bestLength *= 3
		
		if position.lane.endLaneIndex+1 < len(track.lanes):
			rightLength = track.minimumDistanceToEnd(Position(
				pieceIndex = switchIndex,
				inPieceDistance = 0,
				lap = position.lap,
				lane = LanePosition(
					startLaneIndex = position.lane.endLaneIndex,
					endLaneIndex = position.lane.endLaneIndex + 1
				)
			))
			#print "rightLength", switchIndex, rightLength

			if rightLength < bestLength:
				bestLength = rightLength
				bestSwitch = "Right"

		if position.lane.endLaneIndex > 0:
			leftLength = track.minimumDistanceToEnd(Position(
				pieceIndex = switchIndex,
				inPieceDistance = 0,
				lap = position.lap,
				lane = LanePosition(
					startLaneIndex = position.lane.endLaneIndex,
					endLaneIndex = position.lane.endLaneIndex - 1
				)
			))
			#print "leftLength", switchIndex, leftLength

			if leftLength < bestLength:
				bestLength = leftLength
				bestSwitch = "Left"
	
		return bestSwitch
	
	
	def getSuggestedSwitch(self, car):
		"""
		Restituisce lo switch ("Right" | "Left" | None) suggerito.
		"""
		
		position = car.position
		
		# Se davanti ho un avversario lento, cerco di superarlo
		
		track = self.gameData.track
		myCar = self.gameData.getMyCar()
		myCarId = self.gameData.myCarId
		
		for enemyCar in self.gameData.carsByName.values():
			if enemyCar.name != myCarId.name:
				
				myPieceIndex = car.position.pieceIndex
				enemyPieceIndex = enemyCar.position.pieceIndex
				if enemyPieceIndex == ( (myPieceIndex + 1) % len(track.pieces) ) and \
				   enemyCar.position.lane.endLaneIndex == car.position.lane.endLaneIndex and \
				   track.getPiece(enemyPieceIndex).switch:

					# Se l'altro è abbastanza veloce, ignoralo
					if enemyCar.vel >= myCar.vel + 0.3:
						print "Potevo tentare un sorpasso, ma quello davanti è troppo veloce"
						continue

					print "Tento il sorpasso!"
					# L'altro si trova nel piece dopo, che è uno switch
					
					# Da ora assumo che sia un tizio lento
					
					bestLength = 99999999999999
					bestSwitch = None
					switchIndex = enemyPieceIndex
					
					if position.lane.endLaneIndex+1 < len(track.lanes):
						rightLength = track.minimumDistanceToEnd(Position(
							pieceIndex = switchIndex,
							inPieceDistance = 0,
							lap = position.lap,
							lane = LanePosition(
								startLaneIndex = position.lane.endLaneIndex,
								endLaneIndex = position.lane.endLaneIndex + 1
							)
						))

						if rightLength < bestLength:
							bestLength = rightLength
							bestSwitch = "Right"

					if position.lane.endLaneIndex > 0:
						leftLength = track.minimumDistanceToEnd(Position(
							pieceIndex = switchIndex,
							inPieceDistance = 0,
							lap = position.lap,
							lane = LanePosition(
								startLaneIndex = position.lane.endLaneIndex,
								endLaneIndex = position.lane.endLaneIndex - 1
							)
						))

						if leftLength < bestLength:
							bestLength = leftLength
							bestSwitch = "Left"
						
						if bestSwitch is not None:
							print "Tento il sorpasso di %s andando a %s" % (enemyCar.name, bestSwitch)
						
					return bestSwitch
		
		# Calcolo il miglior switch per avere il percorso ottimo
		return self.getSwitchForOptimalPath(position)

	def getLaps(self, car):
		"""
		Restituisce il numero di giri della gara (vero o fittizio).
		"""
		# In QUALIFICATION mode, nei giri dispari (occhio, il primo giro è lo 0) gli facciamo credere che sia l'ultimo
		raceLaps = self.gameData.raceSession.laps
		if self.mode == self.QUALIFICATION or raceLaps is None:
			raceLaps = car.position.lap + 1
			if raceLaps % 2 == 0 and raceLaps > 0:
				raceLaps += 1
			else:
				# Fagli credere di essere al traguardo
				pass
		return raceLaps
	
	def getSafeGlobalCrashAngle(self, car):
		"""
		Restituisce il crashAngle da usare nella simulazione
		"""
		
		crashAngle = 0.99 * self.detective.getCrashAngle()
		
		if self.mode == self.DEDUCTION:
			# Caaauti, non c'è fretta. Ma non troppo o non trova le costanti angolari
			crashAngle *= 0.5
		
		if self.mode == self.SLALOM:
			# Caaauti, non c'è fretta. Ma non troppo o non trova le costanti angolari
			crashAngle *= 0.9
		
		# Euristica stupida per prevedere collisioni LONTANE
		hit_vel = -1
		for enemy in self.gameData.getCarsAhead(car, 100):
			hit_vel = max(hit_vel, car.vel - enemy.vel)
		for enemy in self.gameData.getCarsBehind(car, 100):
			hit_vel = max(hit_vel, enemy.vel - car.vel)
		
		if hit_vel <= 0.5:
			crashAngle *= 1.0
		elif hit_vel <= 1:
			if self.gameData.isQualification():
				crashAngle *= 0.96
			else:
				crashAngle *= 0.94
		elif hit_vel <= 2:
			crashAngle *= 0.9
		elif hit_vel <= 3:
			crashAngle *= 0.8
		elif hit_vel <= 4:
			crashAngle *= 0.7
		elif hit_vel <= 5:
			crashAngle *= 0.6
		else:
			# Qualcuno e' fermo? Sta arrivando qualcuno sparato?
			crashAngle *= 0.5

		# Euristica stupida per prevedere collisioni VICINE
		hit_vel = -1
		for enemy in self.gameData.getCarsAhead(car, 50):
			hit_vel = max(hit_vel, car.vel - enemy.vel)
		for enemy in self.gameData.getCarsBehind(car, 50):
			hit_vel = max(hit_vel, enemy.vel - car.vel)
		
		if hit_vel < 0:
			crashAngle *= 1.0
		elif hit_vel <= 0.5:
			if self.gameData.isQualification():
				crashAngle *= 0.97
			else:
				crashAngle *= 0.96
		
		# Abbassa il counter GLOBALE
		if car.crashCounter <= 5:
			crashAngle *= 1.00
		elif car.crashCounter <= 7:
			crashAngle *= 0.9
		elif car.crashCounter <= 9:
			crashAngle *= 0.8
		elif car.crashCounter <= 10:
			crashAngle *= 0.7
		else:
			# Mio dio, speriamo di non finire mai qui D:
			crashAngle *= 0.6

		return crashAngle
	
	def getSafeTickRelax(self, car):
		"""
		Decidi quanti tick di accelerazione simulare
		Motivo: Se verrà fatto lo switchLane o starTurbo mantiene il throttle di prima
		Minimo 1!
		"""
		
		# Cerchiamo di imparare qualcosa dagli errori
		if car.crashCounter == 0:
			return 1
		elif car.crashCounter == 1:
			return 1
		elif car.crashCounter == 2:
			return 2
		elif car.crashCounter == 3:
			return 2
		elif car.crashCounter == 4:
			return 3
		elif car.crashCounter == 5:
			return 4
		elif car.crashCounter == 6:
			return 5
		elif car.crashCounter == 7:
			return 6
		elif car.crashCounter == 8:
			return 7
		else:
			return 8

	def isTurboAGoodIdea(self, car):
		"""
		Restituisce True se è buona cosa usare il turbo in questa posizione
		Cioè se per almeno tot tick possiamo mettere throttle a 1
		"""
		
		globalCrashAngle = self.getSafeGlobalCrashAngle(car)
		laps = self.getLaps(car)

		# Attiva il turbo
		carCopy = self.simulator.simulateTick(car, TurboAction(message="Test turbo", tick=None), crashAngle=globalCrashAngle)

		# Full throttle
		minFullThrottleTicks = min(20, carCopy.turboAvailable.turboDurationTicks)
		
		carCopy = self.simulator.simulateConstantThrottle(carCopy, 1.0, minFullThrottleTicks, laps, crashAngle=globalCrashAngle)

		# Full brake
		# Il +5 è un margine di sicurezza nel caso getBrakingTime non fosse abbastanza preciso
		numBrakeTicks = 5 + self.detective.getBrakingTime(carCopy.vel, 1)
		numBrakeTicks = max(30, numBrakeTicks) # Limita inferiormente
		numBrakeTicks = min(500, numBrakeTicks) # Limita superiormente

		carCopy = self.simulator.simulateConstantThrottle(carCopy, 0, numBrakeTicks, laps, crashAngle=globalCrashAngle)
		
		# Ok, possiamo accelerare "a tavoletta"
		if carCopy.isSafe():
			return True
		
		# Rettilineo più lungo
		if car.position.pieceIndex == self.gameData.track.longestRun:
			return True
		
		# Se è da tanto che non si sta usando il turbo
		if car.turboReceivedTick is not None and car.tick - car.turboReceivedTick > 500:
		  return True

		# Magari un'altra volta
		return False
	
	def getOptimalThrottle(self, car, laps):
		"""
		Restituisce il più alto valore di throttle che consenta alla macchina di non uscire di pista.
		laps è il numero di giri (se si arriva alla fine prima di crashare, va bene lo stesso!)
		
		Semplicemente, si fa una ricerca binaria sul throttle ottimo, e poi si simula.
		"""
		
		searchInterval = (0.0, 1.0)

		# Decidi quanti tick di frenata simulare
		# Il 5 è un margine di sicurezza nel caso getBrakingTime non fosse abbastanza preciso
		maxVel = car.vel + self.detective.getTanAcc(car.vel, throttle=1, turbo=car.getTurboFactor())
		numBrakeTicks = 5 + self.detective.getBrakingTime(maxVel, 1)
		numBrakeTicks = max(30, numBrakeTicks) # Limita inferiormente
		numBrakeTicks = min(500, numBrakeTicks) # Limita superiormente

		# Decidi quanti tick di accelerazione simulare
		numThrottleTicks = self.getSafeTickRelax(car)

		if numThrottleTicks <= 0:
			print "WARNING: numThrottleTicks in getOptimalThrottle è minore di 1"

		# TODO Decidi la profondità della ricerca binaria
		binarySearchDepth = 12
		
		# Resetto il maxAngle, perché può essere utile conoscere il massimo angolo che si raggiunge
		car2 = copy.copy(car)
		car2.maxAngle = 0
		
		# Decido il crashAngle
		globalCrashAngle = self.getSafeGlobalCrashAngle(car)
		
		# print "CrashAngle settato a %lf" % ( crashAngle * 180 / math.pi )
		
		for i in xrange(binarySearchDepth):
			t = (searchInterval[0] + searchInterval[1]) / 2
			carCopy = self.simulator.simulateConstantThrottle(car2, t, numThrottleTicks, laps, crashAngle=globalCrashAngle)
			carCopy = self.simulator.simulateConstantThrottle(carCopy, 0, numBrakeTicks, laps, crashAngle=globalCrashAngle)
			
			if carCopy.isSafe():
				# Il throttle t è safe
				searchInterval = (t, searchInterval[1])
			else:
				# Il throttle t non è safe
				searchInterval = (searchInterval[0], t)

		t = searchInterval[1]
		carCopy = self.simulator.simulateConstantThrottle(car2, t, numThrottleTicks, laps, crashAngle=globalCrashAngle)
		carCopy = self.simulator.simulateConstantThrottle(carCopy, 0, numBrakeTicks, laps, crashAngle=globalCrashAngle)
		if carCopy.isSafe():
			# Il throttle t è safe
			optimal = t
		else:
			optimal = searchInterval[0]
		
		#print "Stato dopo la simulazione (optimal throttle = %lf):" % optimal
		#carCopy = self.simulator.simulateConstantThrottle(car2, optimal, 1, laps, crashAngle=globalCrashAngle)
		#carCopy = self.simulator.simulateConstantThrottle(car2, optimal, numThrottleTicks, laps, crashAngle=globalCrashAngle)
		#carCopy = self.simulator.simulateConstantThrottle(carCopy, 0, numBrakeTicks, laps, crashAngle=globalCrashAngle)
		#carCopy.printStatus()
		
		# print "OptimalThrottle:", optimal
		return optimal
	
	def getAction(self, tick):
		"""
		La strategia dovrebbe essere la seguente:
		1) Decidi gli switch della gara, scegliendo il percorso di lunghezza minima
		   possibilmente evitando gli avversari
		2) Decidi il miglior throttle
		3) Decidi se usare il turbo
		"""
		
		car = self.gameData.getMyCar()
		track = self.gameData.track
		raceLaps = self.getLaps(car)
		
		# Decide se cambiare corsia
		# in DEDUCTION mode non lo facciamo, per non falsare i dati
		# Non mandarlo al tick zero perchè viene ignorato

		if self.mode != self.DEDUCTION and tick > 3:
			bestSwitch = self.getSuggestedSwitch(car)
			if bestSwitch is not None and car.pendingSwitch != bestSwitch:
				
				# Controllo che questo switch non mi uccida
				# Il 5 è un margine di sicurezza nel caso getBrakingTime non fosse abbastanza preciso
				numBrakeTicks = 5 + self.detective.getBrakingTime(car.vel, 1)
				numBrakeTicks = max(30, numBrakeTicks) # Limita inferiormente
				numBrakeTicks = min(500, numBrakeTicks) # Limita superiormente
				switchAction = SwitchAction(switch = bestSwitch, tick = tick)
				
				if self.simulator.simulateSwitchAndBrake(car, switchAction, numBrakeTicks, raceLaps, self.getSafeGlobalCrashAngle(car)).isSafe():
					car.pendingSwitch = bestSwitch
					return switchAction
				else:
					# print "Avrei voluto switchare, ma la simulazione mi suggerisce di non farlo."
					pass
		
		# Calcolo il miglior nextThrottle
		nextThrottle = self.getOptimalThrottle(car, raceLaps)
		
		# Che ne dici del turbo?
		# in DEDUCTION mode non lo facciamo, per non falsare i dati
		
		"""
		if car.isTurboAvailable():
			print "Turbo is available and",
			if self.isTurboAGoodIdea(car):
				print "a GOOD idea"
			else:
				print "a BAD idea"
		"""
		
		if self.mode != self.DEDUCTION:
			if car.throttle > 0.9999 and nextThrottle > 0.9999 and \
				car.isTurboAvailable() and self.isTurboAGoodIdea(car):
					car.activateTurbo()
					return TurboAction(message = "Warp Speed!!!", tick = tick)

		car.setThrottle(nextThrottle)
		return ThrottleAction(throttle = nextThrottle, tick = tick)
