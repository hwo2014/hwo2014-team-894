#!/bin/bash -e

export TRACKNAME="$1"
SERVER="$2"
PORT="$3"
export CARCOUNT=7

# Non toccare questa variabile
export IL_NOSTRO_TEST=1

if [ -z "$TRACKNAME" ]; then
  TRACKNAME="keimola"
fi

if [ -z "$SERVER" ]; then
  SERVER="senna.helloworldopen.com"
fi

if [ -z "$PORT" ]; then
  PORT="8091"
fi

# Lista dei pid attivi (così sappiamo cosa chiudere)
pids=""

# Esegui la funzione onINT() quando si riceve Ctr+C
onINT() {
	echo -e "\r[*] Received INT signal, exiting..."
	# Chiudi i terminali attivi
	for pid in $pids; do
		kill -INT $pid || true
	done
	# Termina lo script
	exit
}
trap onINT INT

# Fai partire il bot che hosta la partita
xterm -hold -title "Host Bot - $TRACKNAME" -e "cd ./bots/host && ./run $SERVER $PORT HostBot" &
pids="$! $pids"
sleep 2

# Fai partire il traffico
xterm -hold -title "Wild Card Round Bot - $TRACKNAME" -e "cd ./bots/wildcardround && ./run $SERVER $PORT WildCardRoundBot" &
pids="$! $pids"
sleep 2

xterm -hold -title "Qualification Bot 1 - $TRACKNAME" -e "cd ./bots/qualification && ./run $SERVER $PORT QualificationBot1" &
pids="$! $pids"
sleep 2

xterm -hold -title "Qualification Bot 2 - $TRACKNAME" -e "cd ./bots/qualification && ./run $SERVER $PORT QualificationBot2" &
pids="$! $pids"
sleep 2

# Fai partire i bot
xterm -hold -title "Real Bot 1 - $TRACKNAME" -e "./run $SERVER $PORT RealBot1" &
pids="$! $pids"
sleep 2

xterm -hold -title "Real Bot 2 - $TRACKNAME" -e "./run $SERVER $PORT RealBot2" &
pids="$! $pids"
sleep 2

xterm -hold -title "Real Bot 3 - $TRACKNAME" -e "./run $SERVER $PORT RealBot3" &
pids="$! $pids"
sleep 2

# Attendi, controllando ad intervalli di 1 sec la pressione di Ctrl+C
while [ 1 ]; do
	sleep 1;
done
