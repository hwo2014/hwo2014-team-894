import socket
import sys
from src.GameBot import *
from src.Detective import *
from src.Simulator import *
from src.GameData import *
from src.ControlTower import *

if len(sys.argv) != 5:
	print("Usage: ./run host port botname botkey")
else:
	host, port, name, key = sys.argv[1:5]
	print("Connecting with parameters:")
	print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((host, int(port)))

	bot = GameBot(s, name, key)
	detective = Detective()
	simulator = Simulator()
	gameData = GameData()
	controlTower = ControlTower()

	simulator.gameData = gameData
	simulator.detective = detective
	controlTower.gameData = gameData
	controlTower.simulator = simulator
	controlTower.detective = detective
	
	bot.gameData = gameData
	bot.detective = detective
	bot.controlTower = controlTower

	bot.run()
