#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple, deque
import math
import copy

SwitchAction = namedtuple("SwitchAction", ["switch", "tick"])
ThrottleAction = namedtuple("ThrottleAction", ["throttle", "tick"])
TurboAction = namedtuple("TurboAction", ["message", "tick"])

CarId = namedtuple("CarId", ["name", "color"])
Dimensions = namedtuple("Dimensions", ["length", "width", "guideFlagPosition"])
Position = namedtuple("Position", ["pieceIndex", "inPieceDistance", "lane", "lap"])
LanePosition = namedtuple("LanePosition", ["startLaneIndex", "endLaneIndex"])
Turbo = namedtuple("Turbo", ["turboDurationMilliseconds", "turboDurationTicks", "turboFactor"])

class Car(object):

	def __init__(self, gameData, id, dimensions, detective):
		"""
		gameData : gameData
		id : CarId
		dimensions : Dimensions
		"""
		self.gameData = gameData
		self.id = id
		self.dimensions = dimensions
		self.detective = detective
		
		self.crashed = False
		self.disqualified = False
		self.throttle = 0
		self.pendingSwitch = None

		# Quando è TRUE so che che devo gestire i crash come simulazioni e non crash veri
		# In particolare non cambio i crashCounter della macchina e di Track
		#
		# NOTA: Simulator.simulateTick ha un assert che controlla di star lavorando su una
		# macchian di simulazione, quindi dovremmo essere a posto
		self.SIMULATION_CAR = False

		# L'oggetto del turbo in uso o che si può utilizzare
		# None se è gia stato utilizzato o se non è stato ricevuto
		self.turboAvailable = None
		# Il tick al quale abbiamo ricevuto il turbo (+-1)
		self.turboReceivedTick = None
		# Il tick al quale abbiamo mandato lo startTurbo
		# None se non è ancora stato utilizzato
		self.turboStartTick = None
		self.turboActivated = False

		self.previousPosition = None
		self.position = None
		
		self.angle = 0
		self.previousAngle = 0
		self.vel = 0
		self.previousVel = 0
		self.angVel = 0
		self.previousAngVel = 0
		self.acc = 0
		self.angAcc = 0
		
		self.previousCurvature = 0
		
		self.maxAngle = 0 # massimo angolo in valore assoluto
		
		# Flag che indica se la macchina è arrivata al traguardo (usato solo per simulazioni)
		self.arrived = False
		
		self.tick = None
		
		self.crashCounter = 0
	
	def gameInit(self):
		# In una gara del CI alla race era convinto di essere ancora crashato, aggiungo per sicurezza
		self.setCrashed(False)

	@property
	def name(self):
		return self.id.name

	def setCrashed(self, crashed, tick=None):
		self.crashed = crashed
		
		if not crashed:
			# Spawn
			self.setVel(0)
			self.setThrottle(0)
			self.pendingSwitch = None

			self.turboEnd()
			
			# Metto un po' di valori che non rischiano di far saltare il fit
			self.setAngVel(0)
			self.previousCurvature = 0
			self.acc = 0
			self.angAcc = 0

		else:

			# Non ha senso spaventarci se non conosciamo la fisica...
			if not self.SIMULATION_CAR \
			 and self.detective.tanConstantsFound \
			 and self.detective.simpleAngConstantsFound \
			 and self.detective.complexAngConstantsFound:

				curPiece = self.gameData.track.getPiece(self.position.pieceIndex)
				curPiece.lowerCrashAngleFactor(0.95) # Il nuovo crashAngleFactor sul pezzo è il 95% del precedente
				curPiece.crashCounter += 1

				if self.position.inPieceDistance <= 0.5 * curPiece.avgLength:
					prevPiece = self.gameData.track.getPiece(self.position.pieceIndex - 1)
					prevPiece.lowerCrashAngleFactor(0.96) # Il nuovo crashAngleFactor sul pezzo è il 96% del precedente

				self.crashCounter += 1

	def setDisqualified(self, reason, tick=None):
		self.disqualified = True
	
	def updatePosition(self, newAngle, newPosition, tick=None):
		"""
		Assume che venga chiamato ad ogni tick che arriva dal server.
		newAngle : float
		newPosition : Position
		tick : int
		"""
		
		if tick is None:
			# È l'informazione iniziale
			print "Initial position received"
			crashCounter = self.crashCounter
			self.__init__(self.gameData, self.id, self.dimensions, self.detective)
			self.crashCounter = crashCounter
			self.setPosition(newPosition)
			self.setAngle(newAngle)
			return
		
		if self.position is None:
			distance = newPosition.inPieceDistance
			angDistance = newAngle
		elif self.position.pieceIndex != newPosition.pieceIndex:
			# Siamo entrati in un nuovo Piece

			# Calcola la distanza effettiva usando track.getDistance, che sa gestire anche il caso
			# in abbiamo oltrepassato più di un piece in uno stesso tick
			distance = self.gameData.track.getDistance(self.position, newPosition)

			angDistance = newAngle - self.angle

			# Annulla il pendingSwitch se siamo appena entrati in uno switch
			if self.gameData.track.getPiece(self.position.pieceIndex).switch:
				self.pendingSwitch = None
		else:
			distance = newPosition.inPieceDistance - self.position.inPieceDistance
			angDistance = newAngle - self.angle

		newVel = distance
		self.acc = (newVel - self.vel)
		self.setVel(newVel)
		
		newAngVel = angDistance
		self.angAcc = (newAngVel - self.angVel)
		self.setAngVel(newAngVel)
		
		# Salvo la curvatura vecchia appena prima di fare l'update della posizione
		if self.position is not None:
			self.previousCurvature = self.getCurrentCurvature()
		
		self.setPosition(newPosition)
		self.setAngle(newAngle)
		self.tick = tick
	
	def setPosition(self, position):
		self.previousPosition = self.position
		self.position = position

	def setThrottle(self, throttle):
		self.throttle = throttle
	
	def setAngle(self, angle):
		self.previousAngle = self.angle
		self.angle = angle
		self.maxAngle = max( self.maxAngle, abs(angle) )
	
	def setVel(self, vel):
		self.previousVel = self.vel
		self.vel = vel
	
	def setAngVel(self, angVel):
		self.previousAngVel = self.angVel
		self.angVel = angVel
	
	
	def addDistance(self, distance):
		"""
		Fai avanzare la macchina di distance, aggiornando self.position e self.pendingSwitch.
		Utilizza self.pendingSwitch se entra in un pezzo che è switch.
		Viene usato solo per le simulazioni.
		"""
		
		newPosition, self.pendingSwitch = self.gameData.track.addDistance(distance, self.position, self.pendingSwitch)
		self.setPosition(newPosition)
	
	def getCurrentCurvature(self):
		"""
		Ritorna la curvatura del pezzo del circuito in cui ci si trova
		"""
		piece = self.gameData.track.getPiece(self.position.pieceIndex)
		return piece.getCurvature(self.position.lane.startLaneIndex, self.position.lane.endLaneIndex, self.position.inPieceDistance)
	
	def printStatus(self):
		crashed = "OK" if not self.crashed else "CRASHED"
		print "Tick %4d:  Status %9s  Throttle: %1.6lf  Turbo: %1.1lf  Acc: %2.6lf Angle: %2.1lf°  Vel: %3.2lf  AngVel: %3.2lf°  AngAcc: %2.6lf°" % (self.tick or 0, crashed, self.throttle, self.getTurboFactor(), self.acc, self.angle/math.pi*180, self.vel, self.angVel/math.pi*180, self.angAcc/math.pi*180)
		print "  Position:", self.position
		print "  Piece:", self.gameData.track.pieces[self.position.pieceIndex]

	def isTurboAvailable(self):
		return self.turboAvailable is not None and \
		       self.turboStartTick is None and \
		       self.turboActivated is False

	def isTurboActive(self):
		return self.turboAvailable is not None and \
		       (self.turboStartTick is not None or self.turboActivated)

	def getTurboFactor(self):
		if self.isTurboActive():
			return self.turboAvailable.turboFactor
		else:
			return 1.0
	
	def turboStart(self, tick):
		if tick is None:
			print "WARNING: tick in turboStart è None"
		self.turboStartTick = tick
		self.turboActivated = True

	def turboEnd(self, tick = None):
		self.turboAvailable = None
		self.turboReceivedTick = None
		self.turboStartTick = None
	
	def activateTurbo(self):
		# Setto la flag che dice che ho provato ad attivare il turbo
		self.turboActivated = True
	
	def setTurboAvailable(self, turbo):
		# Un nuovo turbo diventa disponibile!
		self.turboAvailable = turbo
		self.turboReceivedTick = self.tick
		self.turboStartTick = None
		self.turboActivated = False
	
	def isSafe(self):
		return self.arrived or not self.crashed
	
	def isSwitching(self):
		return self.position.lane.startLaneIndex != self.position.lane.endLaneIndex
