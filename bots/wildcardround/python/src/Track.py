#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple

from Car import *
from Piece import *

Lane = namedtuple("Lane", ["distanceFromCenter", "index"])

class Track(object):

	def __init__(self, id, name, pieces, lanes, laps=10):
		self.id = id
		self.name = name
		self.pieces = pieces
		self.lanes = lanes
		self.laps = laps

		# indice della partenza della più lunga sequenza (in termini di somma di length) di straightPiece consecutivi
		self.longestRun = None

	def setLaps(self, laps):
		self.laps = laps

		# TODO Svuota la memoization di minimumDistanceToEnd

	def getLap(self, index):
		"""
		Assumendo che index sia il numero di pezzi percorsi (quindi > len(self.pieces))
		restituisce il numero del giro corrente.
		Se non si è ancora completato un giro, ad esempio, restituisce 0.
		"""
		return index // len(self.pieces)
	
	def getPiece(self, index):
		"""
		Assumendo che index sia il numero di pezzi percorsi (quindi > len(self.pieces))
		restituisce il pezzo corrente.
		Se non si è ancora percorso tutto il primo pezzo, ad esempio, restituisce il primo.
		"""
		return self.pieces[index % len(self.pieces)]
	
	def getNextSwitchPieceIndex(self, index):
		"""
		Restituisce l'indice del prossimo pezzo switch.
		"""
		
		currPieceIndex = index
		currPiece = self.getPiece(currPieceIndex)
		
		while currPieceIndex < 999 and not currPiece.switch:
			currPieceIndex += 1
			currPiece = self.getPiece(currPieceIndex)

		return currPieceIndex % len(self.pieces)
	
	def distanceToEnd(self, position):
		"""
		Restituisce la distanza tra position e il traguardo, senza mai cambiare lane.
		TODO: si può fare memoization
		"""

		distance = 0
		currPieceIndex = len(self.pieces)-1
		currPiece = self.getPiece(currPieceIndex)
		
		while position.pieceIndex <= currPieceIndex:
			distance += self.getPiece(currPieceIndex).getLength(position.lane.endLaneIndex)
			currPieceIndex -= 1

		distance -= position.inPieceDistance

		return distance

	def minimumDistanceToEnd(self, position):
		"""
		Restituisce la minima distanza tra position e il traguardo, facendo switch.
		TODO: si può fare memoization
		"""
		pieceIndexPos = position.pieceIndex + position.lap*len(self.pieces)

		distance = [0.0] * len(self.lanes)
		currPieceIndex = self.laps*len(self.pieces)-1
		currPiece = self.getPiece(currPieceIndex)
		canSwitch = currPiece.switch
		
		# Dal traguardo, trova il percorso minimo con una semplice dinamica
		while pieceIndexPos < currPieceIndex:

			# Di base non cambiare corsia
			newDistance = [0.0] * len(self.lanes)
			
			for lane in self.lanes:
				newDistance[lane.index] = distance[lane.index] + currPiece.getLength(lane.index)   # Non cambiare corsia
				if canSwitch and lane.index-1 >= 0:                                                # Vai alla corsia precedente
					newDistance[lane.index] = min(newDistance[lane.index], distance[lane.index-1] + currPiece.getLength(lane.index, lane.index-1))
				if canSwitch and lane.index+1 < len(self.lanes):                                 # Vai alla corsia successiva
					newDistance[lane.index] = min(newDistance[lane.index], distance[lane.index+1] + currPiece.getLength(lane.index, lane.index+1))

			# Prepara i dati per il prossimo giro
			distance = newDistance[:]
			# print distance
			
			# Fai arretrare il puntatore
			currPieceIndex -= 1
			currPiece = self.getPiece(currPieceIndex)
			canSwitch = currPiece.switch

		if currPiece.switch:
			return distance[position.lane.endLaneIndex] \
						+ currPiece.getLength(position.lane.startLaneIndex, position.lane.endLaneIndex) \
						- position.inPieceDistance
		else:
			return distance[position.lane.startLaneIndex] \
						+ currPiece.getLength(position.lane.startLaneIndex) \
						- position.inPieceDistance

	def getDistance(self, startPosition, endPosition):
		"""
		Restituisce la distanza tra startPosition e endPosition.
		Assume che siano sulla stessa lane!
		startPosition : Position
		endPosition : Position
		"""
		try:
			startPieceIndex = startPosition.pieceIndex + startPosition.lap*len(self.pieces)
			endPieceIndex = endPosition.pieceIndex + endPosition.lap*len(self.pieces)
			swapped = False
		
			if startPieceIndex > endPieceIndex:
				swapped = True
				#
				tmp = startPieceIndex
				startPieceIndex = endPieceIndex
				endPieceIndex = tmp
				#
				tmp = startPosition
				startPosition = endPosition
				endPosition = tmp
			

			distance = 0
			currPieceIndex = startPieceIndex
		
			while currPieceIndex < endPieceIndex:
				currPiece = self.getPiece(currPieceIndex)
				distance += currPiece.getLength(startPosition.lane.startLaneIndex, endPosition.lane.endLaneIndex)
				currPieceIndex += 1

			distance -= startPosition.inPieceDistance
			distance += endPosition.inPieceDistance

			if swapped:
				return -distance
			else:
				return distance
		
		except AttributeError:
			print "ECCEZIONE NEL CALCOLO DELLA DISTANZA!"
			return 0

	def addDistance(self, distance, position, pendingSwitch=None):
		"""
		Fai avanzare position di distance, aggiornando.
		Utilizza pendingSwitch se entra in un pezzo che è switch.
		Restituisce i nuovi (position, pendingSwitch)
		"""

		inPieceDistance = position.inPieceDistance + distance
		pieceIndex = position.pieceIndex
		lap = position.lap
		startLaneIndex = position.lane.startLaneIndex
		endLaneIndex = position.lane.endLaneIndex

		pieceLength = self.getPiece(pieceIndex).getLength(startLaneIndex, endLaneIndex)

		while inPieceDistance > pieceLength:
			# Ho finito di percorrere un pezzo (di lunghezza pieceLength)
			inPieceDistance -= pieceLength
			pieceIndex += 1
			
			if pieceIndex >= len(self.pieces):
				pieceIndex -= len(self.pieces)
				lap += 1
			
			# Se ho appena finito uno switch, startLaneIndex diventa uguale a endLaneIndex
			startLaneIndex = endLaneIndex
			
			# Se il pezzo nuovo è uno switch...
			if self.getPiece(pieceIndex).switch:
				if pendingSwitch == "Right":
					endLaneIndex = min(endLaneIndex + 1, len(self.lanes)-1)
					pendingSwitch = None
				elif pendingSwitch == "Left":
					endLaneIndex = max(endLaneIndex - 1, 0)
					pendingSwitch = None

			pieceLength = self.getPiece(pieceIndex).getLength(startLaneIndex, endLaneIndex)

		newLane = LanePosition(startLaneIndex=startLaneIndex, endLaneIndex=endLaneIndex)
		position = Position(pieceIndex=pieceIndex, inPieceDistance=inPieceDistance, lane=newLane, lap=lap)

		return (position, pendingSwitch)
