#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple, deque
import math
import numpy
import copy
from numpy.linalg import lstsq
from Car import Position
from Piece import StraightPiece, BendPiece
#from scipy.optimize import curve_fit
import pickle
import bisect

TangentialDataPoint = namedtuple("TangentialDataPoint", ["throttle", "velocity", "effTanAcc"])
AngularDataPoint = namedtuple("AngularDataPoint", ["curvature", "carAngle", "velocity", "angularVelocity", "effAngAcc"])
SwitchDataPoint = namedtuple("SwitchDataPoint", ["inPieceDistance", "carAngle", "velocity", "angularVelocity", "effAngAcc"])

class Detective(object):
	
	def __init__(self):
		# Costanti per calcolare l'accelerazione tangenziale
		
		# Valori attorno ai quali oscillano quelli veri:
		self.fixedTanAccMax = 0.2
		self.fixedTanVelMax = 10.0
		
		# Variazioni massime consentite alle costanti, rispetto ai valori qua sopra:
		self.tanAccMaxVariation = 0.19
		self.tanVelMaxVariation = 9.0
		# TODO: decidere se siamo contenti di queste variazioni qua sopra
		
		# Guess iniziale sulle costanti:
		self.tanAccMax = self.fixedTanAccMax
		self.tanVelMax = self.fixedTanVelMax
		
		self.tanData = deque() # Dati memorizzati

		# Costanti per calcolare l'accelerazione angolare
		
		self.angVelConstant = -0.1
		self.unphysicalConstant = -0.00125 # Costante che moltiplica velocity*angle
		
		self.vvkConstant = 0.009 # Costante che moltiplica v*v*sqrt(k)
		self.vConstant = -0.005 # Costante che moltiplica v
		
		# self.safeThreshold = 0.20 # Stima dal basso del threshold: è importante che il threshold non sia più basso di così!
		# TODO: determinare un safeThreshold
		
		self.angData = deque()
		self.straightAngData = deque()		# Dati relativi a tratti dritti di strada, con angoli diversi da 0
		self.nonStraightAngData = deque()	# Dati relativi a tratti NON dritti di strada
		self.switchAngData = dict()			# Dati relativi a particolari switch
		
		# Variabili che indicano se le costanti sono state trovate
		self.tanConstantsFound = False
		self.simpleAngConstantsFound = False
		self.complexAngConstantsFound = False

		# Angolo oltre il quale la macchina crasha
		self.crashAngle = 60 * math.pi / 180
	
	
	def seenSomeoneElse(self, car):
		"""
		Gli viene passata una macchina car **degli avversari** non crashata e senza altri vicini.
		car viene utilizzata soltanto per avere lo stato della macchina, non per chiamarci metodi.
		Non memorizza tutto, ma screma e tiene i dati importanti
		"""

		pass
		
	def seenMyself(self, car):
		"""
		Gli viene passata **la nostra macchina** car non crashata e senza altri vicini.
		car viene utilizzata soltanto per avere lo stato della macchina, non per chiamarci metodi.
		Non memorizza tutto, ma screma e tiene i dati importanti
		"""
		
		if not car.isTurboActive():
			# Analisi dell'accelerazione tangenziale
			
			self.tanData.append( TangentialDataPoint(throttle = car.throttle, velocity = car.previousVel, effTanAcc = car.acc) )
			
			# Se ci sono tanti valori, buttiamo via il più vecchio e tanti saluti
			if len(self.tanData) > 30:
				self.tanData.popleft()
			# if len(self.tanData) == 1000: pickle.dump(self.tanData, open("data.txt", "w"))
			
			# Fai regressione lineare sulle grandezze tangenziali (metodo dei minimi quadrati)
			if len(self.tanData) > 10:
				A = [[p.throttle, p.velocity] for p in self.tanData]
				B = [p.effTanAcc for p in self.tanData]
				
				res = lstsq(A, B)
				X = res[0]

				try:
					residual = res[1][0]
				except IndexError:
					residual = 99999999
				
				tanAccMax = X[0]
				tanVelMax = (-1/X[1])*X[0]
				
				# Sovrascrivo le costanti solo se il fit è molto buono
				if residual < 1e-20:
					

					# Cambiamo le costanti solo se sono nel "range di credibilità"
					if abs( tanAccMax - self.fixedTanAccMax ) < self.tanAccMaxVariation and abs( tanVelMax - self.fixedTanVelMax ) < self.tanVelMaxVariation:
						if not self.tanConstantsFound:
							print "*** Nuove costanti tangenziali: %lf, %lf ***" % (self.tanAccMax, self.tanVelMax)
							self.tanAccMax = tanAccMax
							self.tanVelMax = tanVelMax
							self.tanConstantsFound = True
					else:
						print "*** Le costanti trovate non sono nell'intervallo di credibilità: %lf, %lf ***" % (tanAccMax, tanVelMax)

		# Analisi della lunghezza degli switch
		if self.tanConstantsFound and not car.isTurboActive():
			previousPosition = car.previousPosition
			
			if previousPosition \
				and previousPosition.lane.startLaneIndex != previousPosition.lane.endLaneIndex \
				and previousPosition.pieceIndex != car.position.pieceIndex: # La macchina è appena uscita dallo switch
				
				realTanAcc = self.getTanAcc(car.previousVel, car.throttle, car.getTurboFactor())
				projectedPosition = previousPosition.inPieceDistance + car.previousVel + realTanAcc

				fakeStartPosition = Position(
					pieceIndex = (previousPosition.pieceIndex + 1) % len(car.gameData.track.pieces),
					inPieceDistance = 0.0,
					lane = car.position.lane,
					lap = previousPosition.lap + (((previousPosition.pieceIndex + 1) % len(car.gameData.track.pieces)) == 0)
				)
				overshoot = car.gameData.track.getDistance(fakeStartPosition, car.position)

				switchLength = projectedPosition - overshoot

				print "** Switch length =", switchLength

				previousPiece = car.gameData.track.getPiece(previousPosition.pieceIndex)
				if isinstance(previousPiece, StraightPiece):
					previousPiece.switchLength = switchLength
				elif isinstance(previousPiece, BendPiece):
					previousPiece.switchLength[(previousPosition.lane.startLaneIndex, previousPosition.lane.endLaneIndex)] = switchLength
					previousPiece.switchLength[(previousPosition.lane.endLaneIndex, previousPosition.lane.startLaneIndex)] = switchLength
		
	
	def seenSomebody(self, car):
		"""
		Gli viene passata una car non crashata e senza altri vicini, nostra o anche non nostra.
		car viene utilizzata soltanto per avere lo stato della macchina, non per chiamarci metodi.
		Non memorizza tutto, ma screma e tiene i dati importanti
		"""


		# Analisi dell'accelerazione angolare
		point = AngularDataPoint(curvature = car.previousCurvature, carAngle = car.previousAngle, velocity = car.previousVel, \
			angularVelocity = car.previousAngVel, effAngAcc = car.angAcc)
		
		self.angData.append(point)
		if len(self.angData) > 400:
			self.angData.popleft()

		if abs(car.previousCurvature) <= 1e-10 and abs(car.previousAngle) > 1e-3:
			self.straightAngData.append(point)
			# Tengo solo 30 valori su cui fare il fit.
			if len(self.straightAngData) > 30:
				self.straightAngData.popleft()
		if abs(car.previousCurvature) >= 1e-5:
			self.nonStraightAngData.append(point)
			if len(self.nonStraightAngData) > 200:
				self.nonStraightAngData.popleft()
		
		if len(self.straightAngData) > 20:
			# Costanti angolari semplici
			A = [ [ p.angularVelocity, p.velocity * p.carAngle ] for p in self.straightAngData ]
			B = [ p.effAngAcc for p in self.straightAngData ]
			
			res = lstsq(A, B)
			X = res[0]

			# print res
			
			try:
				residual = res[1][0]
			except IndexError:
				residual = 99999999
			
			angVelConstant = X[0]
			unphysicalConstant = X[1]
			
			if residual < 1e-20:
				# TODO: fare un check di credibilità
				
				if not self.simpleAngConstantsFound:
					self.angVelConstant = angVelConstant
					self.unphysicalConstant = unphysicalConstant
					print "*** Nuove costanti angolari S: %lf, %lf ***" % (angVelConstant, unphysicalConstant)
					self.simpleAngConstantsFound = True
		
		if self.simpleAngConstantsFound:
			# Altre costanti angolari
			diffs = [ (p.effAngAcc - self.angVelConstant * p.angularVelocity - self.unphysicalConstant * p.velocity * p.carAngle) \
				for p in self.angData ]
			# sortedAngData = sorted(self.angData, key = lambda x: x.velocity ** 2 * abs(x.curvature))
			
			# Prendo solo i dati che danno una differenza diversa da zero
			nonZeroAngData = [ p for p in self.nonStraightAngData if \
				abs(p.effAngAcc - self.angVelConstant * p.angularVelocity - self.unphysicalConstant * p.velocity * p.carAngle) > 10e-10 ]
			
			# print len(nonZeroAngData)

			if len(nonZeroAngData) > 30:
				A = [[ p.velocity ** 2 * math.sqrt( abs(p.curvature) ) * numpy.sign(p.curvature), p.velocity * numpy.sign(p.curvature) ] \
					for p in nonZeroAngData ]
				B = [ p.effAngAcc - self.angVelConstant * p.angularVelocity - self.unphysicalConstant * p.velocity * p.carAngle \
					for p in nonZeroAngData ]
			
				res = lstsq(A, B)
				X = res[0]
			
				try:
					residual = res[1][0]
				except IndexError:
					residual = 99999999
			
				vvkConstant = X[0]
				vConstant = X[1]
				
				if residual < 1e-20:
					# TODO: fare un check di credibilità
					
					# Modifichiamo le costanti solo se non sono già trovate, per diminuire i guai con gli switch.
					# In realtà, tutto questo lavoro lo facciamo anche per macchine altrui, per le quali non abbiamo
					# la garanzia che non vengano fatti switch... Che il server ci assista.
					if not self.complexAngConstantsFound:
						self.vvkConstant = vvkConstant
						self.vConstant = vConstant
						print "*** Nuove costanti angolari C: %lf, %lf ***" % (vvkConstant, vConstant)
						self.complexAngConstantsFound = True
		
		
		# Analisi della curvatura degli switch
		
		previousPosition = car.previousPosition
		
		if previousPosition is not None:
			pieceIndex = previousPosition.pieceIndex
		
			# Controlliamo che la macchina stia facendo uno switch
			if previousPosition.lane.startLaneIndex != previousPosition.lane.endLaneIndex:
			
				# Riempio self.switchAngData con i dati grezzi
				if not pieceIndex in self.switchAngData.keys():
					self.switchAngData[pieceIndex] = []
			
				self.switchAngData[pieceIndex].append( SwitchDataPoint( \
					inPieceDistance = previousPosition.inPieceDistance, \
					carAngle = car.previousAngle, \
					velocity = car.previousVel, \
					angularVelocity = car.previousAngVel, \
					effAngAcc = car.angAcc ) )
		
			# Se abbiamo già trovato le costanti, processiamo i dati del pezzo corrente
			if self.simpleAngConstantsFound and self.complexAngConstantsFound:
			
				# Mi faccio dare la curvatura in una delle lane (startLane), senza tener conto dello switch
				piece = car.gameData.track.getPiece(pieceIndex)
				classicalCurvature = piece.getCurvature(previousPosition.lane.startLaneIndex)
			
				while pieceIndex in self.switchAngData.keys() and len(self.switchAngData[pieceIndex]) > 0:
					point = self.switchAngData[pieceIndex].pop()
				
					# Calcolo l'accelerazione teorica, supponendo non ci siano stati switch
					theoreticalAngAcc = self.getAngAcc( classicalCurvature, point.carAngle, point.velocity, point.angularVelocity )
				
					# Calcolo la differenza tra accelerazione teorica e accelerazione misurata
					difference = point.effAngAcc - theoreticalAngAcc
				
					if abs(difference) > 1e-10:
						
						try:
							effectiveCurvatureSqrt = numpy.sign(classicalCurvature) * difference / (self.vvkConstant * point.velocity**2) + math.sqrt(abs(classicalCurvature))
							if effectiveCurvatureSqrt > 0.0:
								effectiveCurvature = numpy.sign(classicalCurvature) * ( effectiveCurvatureSqrt ** 2 )
						
								# Salvo il valore trovato, se non ci sono già troppi valori
								if len(piece.switchCurvature) < 100:
									bisect.insort_left(piece.switchCurvature, (previousPosition.inPieceDistance, effectiveCurvature))
								# print "SwitchCurvature:", piece.switchCurvature
						
						except Exception:
							pass
					

	def getStaticFrictionTerm(self, velocity, curvature):
		v = velocity
		k = curvature
		return self.vvkConstant * v * v * math.sqrt( abs(k) ) + self.vConstant * v

	def getTanAcc(self, velocity, throttle, turbo=1.0):
		"""
		Restituisce l'accelerazione tangenziale stimata in base alla fisica conosciuta.
		Si assume che dipenda solo da velocity e dal throttle attuale, oltre che dal turbo.
		"""
		
		return self.tanAccMax * throttle * turbo - self.tanAccMax * velocity / self.tanVelMax
	
	def getAngAcc(self, curvature, carAngle, velocity, angularVelocity):
		"""
		Restituisce l'accelerazione angolare stimata in base alla fisica conosciuta.
		Si assume che dipenda solo da curvature, carAngle, velocity e angularVelocity.
		Si assume che gli angoli siano in radianti.
		"""
		
		k = curvature
		v = velocity

		if self.getStaticFrictionTerm(v, k) > 0.0:
			a = numpy.sign(k) * ( self.vvkConstant * v * v * math.sqrt( abs(k) ) + self.vConstant * v )
		else:
			a = 0.0

		return ( self.angVelConstant * angularVelocity + self.unphysicalConstant * velocity * carAngle + a )

	def getCrashAngle(self):
		# TODO: dedurre l'angolo soglia a partire da ciò che si osserva
		# A quanto pare è sempre lo stesso (per fortuna)

		return self.crashAngle

	def getBrakingTime(self, startVelocity, endVelocity):
		"""
		Restituisce il minimo numero (integer) di tick necessari per frenare da startVelocity a endVelocity
		(limitata ad un valore massimo di 3000, perchè usiamo un loop).
		La frenata massima si assume che sia per throttle = 0.
		Restituisce 0 se startVelocity <= endVelocity.
		
		Si assume che la formula per getTanAcc() sia quella che abbiamo trovato, ovvero:
		self.tanAccMax * (throttle - velocity / self.tanVelMax )
		
		Non serve tenere conto del turbo, perché per throttle = 0 non ha alcuna influenza.
		"""

		if startVelocity == 0:
			return 0
		
		if startVelocity <= endVelocity:
			return 0
		
		time = 0
		
		while startVelocity > endVelocity and time < 500 + 10:
			startVelocity += self.getTanAcc(startVelocity, throttle=0)
			time += 1

		#time = self.tanVelMax / self.tanAccMax * math.log( startVelocity / endVelocity )
		return time
