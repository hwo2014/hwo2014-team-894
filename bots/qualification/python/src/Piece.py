#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division


class BasePiece(object):
	def __init__(self):
		pass

class StraightPiece(BasePiece):
	def __init__(self, lanes, absoluteStartAngle, length, switch=False):
		self.lanes = lanes
		self.absoluteStartAngle = absoluteStartAngle
		self.length = length
		self.switch = switch
		self.switchLength = length

	def getLength(self, startLane, endLane=None):
		if endLane == None: endLane = startLane
		elif startLane != endLane:
			return self.switchLength

		return self.length

	def getCurvature(self, startLane, endLane=None):
		if endLane == None: endLane = startLane
		# TODO e se è uno switch?
		
		return 0

	def __repr__(self):
		return "STRAIGHT PIECE\tlength: %ld, lanes: %s, switchLength: %s" % (self.length, self.lanes, self.switchLength)


class BendPiece(BasePiece):
	def __init__(self, lanes, absoluteStartAngle, radius, bendAngle, switch=False):
		self.lanes = lanes
		self.absoluteStartAngle = absoluteStartAngle
		self.radius = radius
		self.bendAngle = bendAngle
		self.switch = switch
		self.switchLength = {}

	def getAbsoluteRadius(self, startLane, endLane=None):
		"""
		Restituisce il raggio (valore assoluto) della curva alla corsia lane
		Solleva una eccezione se pieceIndex non è una curva
		"""
		
		if endLane == None: endLane = startLane
		# TODO e se è uno switch?
		
		if self.bendAngle > 0:
			return self.radius - self.lanes[startLane].distanceFromCenter
		else:
			return self.radius + self.lanes[startLane].distanceFromCenter

	def getSignedRadius(self, startLane, endLane=None):
		"""
		Restituisce il raggio (con segno) della curva pieceIndex alla corsia lane
		Solleva una eccezione se pieceIndex non è una curva
		
		positivo = gira verso destra
		negativo = gira verso sinistra
		"""
		if endLane == None: endLane = startLane
		# TODO e se è uno switch?
		
		absRadius = self.getAbsoluteRadius(startLane, endLane)
		if self.bendAngle > 0:
			return absRadius
		else:
			return -absRadius

	def getBendAngle(self):
		return self.bendAngle
	
	def getLength(self, startLane, endLane=None):
		if endLane == None: endLane = startLane
		elif startLane != endLane and (startLane, endLane) in self.switchLength.keys():
			return self.switchLength[(startLane, endLane)]

		return self.getAbsoluteRadius(startLane) * abs(self.bendAngle)

	def getCurvature(self, startLane, endLane=None):
		if endLane == None: endLane = startLane
		# TODO e se è uno switch?
		
		return 1/self.getSignedRadius(startLane, endLane)

	def __repr__(self):
		return "BEND PIECE\tcentral length:%lf, radius: %lf, bendAngle: %lf, lanes: %s, switchLenght: %s" % (abs(self.bendAngle * self.radius), self.radius, self.bendAngle, self.lanes, self.switchLength)
