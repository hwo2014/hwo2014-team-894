#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple

RaceSession = namedtuple("RaceSession", ["laps", "maxLapTimeMs", "quickRace", "durationMs"])

class GameData(object):

	def __init__(self):
		self.myCarId = None
		self.track = None
		self.carsByName = {}
		self.raceSession = None
		
	def getCars(self):
		return list(self.carsByName.values())

	def addCar(self, car):
		self.carsByName[car.name] = car

	def getCarByName(self, carName):
		return self.carsByName[carName]

	def getMyCar(self):
		return self.getCarByName(self.myCarId.name)
	
	def isMyCar(self, car):
		return car.id.name == self.myCarId.name

	def getCarsNear(self, car, distance):
		"""
		Restituisce le macchine che
		- distano da car meno di distance
		- sono sulla stessa lane (TODO)
		Cioè le macchine che potenzialmente possono essere urtate.
		"""

		myCar = self.getMyCar()
		near = []
		
		for car in self.carsByName.values():
			if car.name != self.myCarId.name:
				if abs( self.track.getDistance(car.position, myCar.position) ) <= distance or abs( self.track.getDistance(myCar.position, car.position) ) <= distance:
					near.append(car)

		return near
