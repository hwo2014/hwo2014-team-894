#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple
import random
import operator
import numpy
import math
import copy

from Car import *
from Piece import *

class ControlTower(object):
	
	# Possibili modalità / tipi di gara
	DEDUCTION = 'Deduction'			# Modalità di deduzione delle costanti
	QUALIFICATION = 'Qualification'	# Qualifiche
	RACE = 'Race'					# Gara vera e propria
	FREE = 'Free'					# Prove libere (quickrace)
	
	def gameInit(self):
		"""
		Inizializza la modalità (viene chiamata da GameBot quando viene ricevuto l'evento gameInit)
		"""
		raceSession = self.gameData.raceSession
		if raceSession.quickRace is None:
			self.gameType = self.QUALIFICATION
			self.mode = self.DEDUCTION
		elif raceSession.quickRace:
			self.gameType = self.FREE
			self.mode = self.DEDUCTION
		else:
			self.gameType = self.RACE
			self.mode = self.RACE
		
		print "Initialized ControlTower Mode: %s/%s" % (self.gameType, self.mode)
	
	def checkMode(self):
		"""
		Dovrebbe essere chiamata periodicamente.
		Aggiorna la modalità, se necessario
		"""
		
		if self.mode == self.DEDUCTION:
			# Controllo se le costanti fisiche sono state trovate
			constantsFound = self.detective.tanConstantsFound \
				and self.detective.simpleAngConstantsFound \
				and self.detective.complexAngConstantsFound
			if constantsFound or self.gameData.getMyCar().position.lap > 1:
				# Passo alla modalità giusta
				if self.gameType == self.QUALIFICATION:
					self.mode = self.QUALIFICATION
				elif self.gameType == self.FREE:
					self.mode = self.RACE
				elif self.gameType == self.RACE:
					# Questo in teoria non dovrebbe accadere
					self.mode = self.RACE
				print "ControlTower Mode changed: %s/%s" % (self.gameType, self.mode)
	
	def __init__(self):
		self.gameData = None
		self.simulator = None
		self.detective = None
		
		# Modalità corrente (DEDUCTION / QUALIFICATION / RACE)
		self.mode = None
		# Tipo di gara (FREE / QUALIFICATION / RACE)
		self.gameType = None
	
	
	def getSwitchForOptimalPath(self, position):
		"""
		Restituisce lo switch ("Right" | "Left" | None) che serve a percorrere il cammino di lunghezza minima.
		"""
		
		track = self.gameData.track

		switchIndex = track.getNextSwitchPieceIndex(position.pieceIndex)

		bestSwitch = None
		bestLength = track.minimumDistanceToEnd(Position(
			pieceIndex = switchIndex,
			inPieceDistance = 0,
			lap = position.lap,
			lane = LanePosition(
				startLaneIndex = position.lane.endLaneIndex,
				endLaneIndex = position.lane.endLaneIndex
			)
		))
		#print "currLength", switchIndex, bestLength

		if position.lane.endLaneIndex+1 < len(track.lanes):
			rightLength = track.minimumDistanceToEnd(Position(
				pieceIndex = switchIndex,
				inPieceDistance = 0,
				lap = position.lap,
				lane = LanePosition(
					startLaneIndex = position.lane.endLaneIndex,
					endLaneIndex = position.lane.endLaneIndex + 1
				)
			))
			#print "rightLength", switchIndex, rightLength

			if rightLength < bestLength:
				bestLength = rightLength
				bestSwitch = "Right"

		if position.lane.endLaneIndex > 0:
			leftLength = track.minimumDistanceToEnd(Position(
				pieceIndex = switchIndex,
				inPieceDistance = 0,
				lap = position.lap,
				lane = LanePosition(
					startLaneIndex = position.lane.endLaneIndex,
					endLaneIndex = position.lane.endLaneIndex - 1
				)
			))
			#print "leftLength", switchIndex, leftLength

			if leftLength < bestLength:
				bestLength = leftLength
				bestSwitch = "Left"
	
		return bestSwitch
	
	
	def getSuggestedSwitch(self, car):
		"""
		Restituisce lo switch ("Right" | "Left" | None) suggerito.
		"""
		
		position = car.position
		
		# Se davanti ho un avversario lento, cerco di superarlo
		
		track = self.gameData.track
		myCarId = self.gameData.myCarId
		
		for enemyCar in self.gameData.carsByName.values():
			if enemyCar.name != myCarId.name:
				
				myPieceIndex = car.position.pieceIndex
				enemyPieceIndex = enemyCar.position.pieceIndex
				if enemyPieceIndex == ( (myPieceIndex + 1) % len(track.pieces) ) and \
				   enemyCar.position.lane.endLaneIndex == car.position.lane.endLaneIndex and \
				   track.getPiece(enemyPieceIndex).switch:

					print "Tento il sorpasso!"
					# L'altro si trova nel piece dopo, che è uno switch
					
					# Assumo che sia un tizio lento
					
					bestLength = 99999999999999
					bestSwitch = None
					switchIndex = enemyPieceIndex
					
					if position.lane.endLaneIndex+1 < len(track.lanes):
						rightLength = track.minimumDistanceToEnd(Position(
							pieceIndex = switchIndex,
							inPieceDistance = 0,
							lap = position.lap,
							lane = LanePosition(
								startLaneIndex = position.lane.endLaneIndex,
								endLaneIndex = position.lane.endLaneIndex + 1
							)
						))

						if rightLength < bestLength:
							bestLength = rightLength
							bestSwitch = "Right"

					if position.lane.endLaneIndex > 0:
						leftLength = track.minimumDistanceToEnd(Position(
							pieceIndex = switchIndex,
							inPieceDistance = 0,
							lap = position.lap,
							lane = LanePosition(
								startLaneIndex = position.lane.endLaneIndex,
								endLaneIndex = position.lane.endLaneIndex - 1
							)
						))

						if leftLength < bestLength:
							bestLength = leftLength
							bestSwitch = "Left"
						
						if bestSwitch is not None:
							print "Sorpasso %s andando a %s" % (enemyCar.name, bestSwitch)
						
					return bestSwitch
		
		
		# Calcolo il miglior switch per avere il percorso ottimo
		return self.getSwitchForOptimalPath(position)

	def getLaps(self, car):
		"""
		Restituisce il numero di giri della gara (vero o fittizio).
		"""
		# In QUALIFICATION mode, nei giri dispari (occhio, il primo giro è lo 0) gli facciamo credere che sia l'ultimo
		raceLaps = self.gameData.raceSession.laps
		if self.mode == self.QUALIFICATION or raceLaps is None:
			raceLaps = car.position.lap + 1
			if raceLaps % 2 == 0 and raceLaps > 0:
				raceLaps += 1
		return raceLaps
	
	def getSafeCrashAngle(self, car):
		"""
		Restituisce il crashAngle da usare nella simulazione
		"""
		
		crashAngle = self.detective.getCrashAngle()
		
		if self.mode == self.DEDUCTION:
			crashAngle *= 0.75
		else:
			crashAngle *= 0.95

		if car.isSwitching():
			crashAngle *= 0.9
		
		if car.crashCounter == 0:
			crashAngle *= 0.95/0.95
		elif car.crashCounter == 1:
			crashAngle *= 0.9/0.95
		elif car.crashCounter == 2:
			crashAngle *= 0.85/0.95
		elif car.crashCounter == 3:
			crashAngle *= 0.8/0.95
		else:
			crashAngle *= ( 1-1/(1+math.exp(-0.3 * car.crashCounter)) )*3/ 0.95

		return crashAngle
	
	def getSafeTickRelax(self, car):
		"""
		Decidi quanti tick di accelerazione simulare
		Motivo: Se verrà fatto lo switchLane o starTurbo mantiene il throttle di prima
		Minimo 1 o 2
		"""
		return 1 + min(car.crashCounter, 10)

	def isTurboAGoodIdea(self, car):
		"""
		Restituisce True se è buona cosa usare il turbo in questa posizione
		Cioè se per almeno 60 tick possiamo mettere throttle a 1
		"""
		
		crashAngle = self.getSafeCrashAngle(car)
		laps = self.getLaps(car)

		# Attiva il turbo
		car = self.simulator.simulateTick(car, TurboAction(message="Test turbo", tick=None), crashAngle=crashAngle)

		# Full throttle
		minFullThrottleTicks = min(50, car.turboAvailable.turboDurationTicks)
		
		car = self.simulator.simulateConstantThrottle(car, 1.0, minFullThrottleTicks, laps, crashAngle=crashAngle)

		# Full brake
		numBrakeTicks = self.detective.getBrakingTime(car.vel, 0.1)
		numBrakeTicks = max(30, numBrakeTicks) # Limita inferiormente
		numBrakeTicks = min(1000, numBrakeTicks) # Limita superiormente

		car = self.simulator.simulateConstantThrottle(car, 0, numBrakeTicks, laps, crashAngle=crashAngle)
		
		return car.isSafe()
	
	def getOptimalThrottle(self, car, laps):
		"""
		Restituisce il più alto valore di throttle che consenta alla macchina di non uscire di pista.
		laps è il numero di giri (se si arriva alla fine prima di crashare, va bene lo stesso!)
		
		Semplicemente, si fa una ricerca binaria sul throttle ottimo, e poi si simula.
		"""
		
		searchInterval = (0.0, 1.0)

		# Decidi quanti tick di frenata simulare
		numBrakeTicks = self.detective.getBrakingTime(car.vel, 0.1)
		numBrakeTicks = max(30, numBrakeTicks) # Limita inferiormente
		numBrakeTicks = min(1000, numBrakeTicks) # Limita superiormente

		# Decidi quanti tick di accelerazione simulare
		numThrottleTicks = self.getSafeTickRelax(car)

		assert(numThrottleTicks>0)

		# TODO Decidi la profondità della ricerca binaria
		binarySearchDepth = 20
		
		# Resetto il maxAngle, perché può essere utile conoscere il massimo angolo che si raggiunge
		car2 = copy.copy(car)
		car2.maxAngle = 0
		
		# Decido il crashAngle
		crashAngle = self.getSafeCrashAngle(car)
		
		# print "CrashAngle settato a %lf" % ( crashAngle * 180 / math.pi )
		
		for i in xrange(binarySearchDepth):
			t = (searchInterval[0] + searchInterval[1]) / 2
			carCopy = self.simulator.simulateConstantThrottle(car2, t, numThrottleTicks, laps, crashAngle=crashAngle)
			carCopy = self.simulator.simulateConstantThrottle(carCopy, 0, numBrakeTicks, laps, crashAngle=crashAngle)
			
			if carCopy.isSafe():
				# Il throttle t è safe
				searchInterval = (t, searchInterval[1])
			else:
				# Il throttle t non è safe
				searchInterval = (searchInterval[0], t)

		t = searchInterval[1]
		carCopy = self.simulator.simulateConstantThrottle(car2, t, numThrottleTicks, laps, crashAngle=crashAngle)
		carCopy = self.simulator.simulateConstantThrottle(carCopy, 0, numBrakeTicks, laps, crashAngle=crashAngle)
		if carCopy.isSafe():
			# Il throttle t è safe
			optimal = t
		else:
			optimal = searchInterval[0]
		
		#print "Stato alla fine della simulazione (optimal throttle = %lf):" % optimal
		#carCopy = self.simulator.simulateConstantThrottle(car2, optimal, numThrottleTicks, laps, crashAngle=crashAngle)
		#carCopy = self.simulator.simulateConstantThrottle(carCopy, 0, numBrakeTicks, laps, crashAngle=crashAngle)
		#carCopy.printStatus()
		
		# print "OptimalThrottle:", optimal
		return optimal
	
	def getAction(self, tick):
		"""
		La strategia dovrebbe essere la seguente:
		1) Decidi tutti gli switch della gara, scegliendo ad esempio il percorso di lunghezza minima
		   (non minimo tempo, difficilissimo da trovare)
		2) Decidi il throttle
		3) Decidi se usare il turbo
		"""
		
		car = self.gameData.getMyCar()
		track = self.gameData.track
		
		# Decide se cambiare corsia
		# in DEDUCTION mode non lo facciamo, per non falsare i dati
		
		if self.mode != self.DEDUCTION:
			bestSwitch = self.getSuggestedSwitch(car)
			if bestSwitch is not None and car.pendingSwitch != bestSwitch:
				car.setThrottle(car.throttle)
				car.pendingSwitch = bestSwitch
				
				# Controllo che questo switch non mi uccida
				numBrakeTicks = self.detective.getBrakingTime(car.vel, 0.1)
				switchAction = SwitchAction(switch = bestSwitch, tick = tick)
				
				if self.simulator.simulateSwitchAndBrake(car, switchAction, numBrakeTicks, self.getLaps(car), self.getSafeCrashAngle(car)).isSafe():
					return switchAction
				else:
					print "Avrei voluto switchare, ma la simulazione mi suggerisce di non farlo."
		
		raceLaps = self.getLaps(car)
		
		# Calcolo il nextThrottle
		nextThrottle = self.getOptimalThrottle(car, raceLaps)
		
		# Che ne dici del turbo?
		# in DEDUCTION mode non lo facciamo, per non falsare i dati
		
		if self.mode != self.DEDUCTION:
			if car.throttle == 1 and \
				nextThrottle == 1 and \
				car.isTurboAvailable() and \
				self.isTurboAGoodIdea(car):
					car.activateTurbo()
					return TurboAction(message = "Warp Speed!!!", tick = tick)

		car.setThrottle(nextThrottle)
		return ThrottleAction(throttle = nextThrottle, tick = tick)
