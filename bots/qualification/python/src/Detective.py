#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Divisioni con la virgola
from __future__ import division
from collections import namedtuple, deque
import math
import numpy
import copy
from numpy.linalg import lstsq
from Car import Position
from Piece import StraightPiece, BendPiece
#from scipy.optimize import curve_fit
import pickle

TangentialDataPoint = namedtuple("TangentialDataPoint", ["throttle", "velocity", "effTanAcc"])
AngularDataPoint = namedtuple("AngularDataPoint", ["curvature", "carAngle", "velocity", "angularVelocity", "effAngAcc"])

class Detective(object):
	
	def __init__(self):
		# Costanti per calcolare l'accelerazione tangenziale
		
		# Valori attorno ai quali oscillano quelli veri:
		self.fixedTanAccMax = 0.2
		self.fixedTanVelMax = 10.0
		
		# Variazioni massime consentite alle costanti, rispetto ai valori qua sopra:
		self.tanAccMaxVariation = 0.02
		self.tanVelMaxVariation = 2.0
		# TODO: decidere se siamo contenti di queste variazioni qua sopra
		
		# Guess iniziale sulle costanti:
		self.tanAccMax = self.fixedTanAccMax
		self.tanVelMax = self.fixedTanVelMax
		
		self.tanData = deque() # Dati memorizzati

		# Costanti per calcolare l'accelerazione angolare
		
		self.angVelConstant = -0.1
		self.unphysicalConstant = -0.00125 # Costante che moltiplica velocity*angle
		
		self.vvkConstant = 0.009 # Costante che moltiplica v*v*sqrt(k)
		self.vConstant = -0.005 # Costante che moltiplica v
		
		# self.safeThreshold = 0.20 # Stima dal basso del threshold: è importante che il threshold non sia più basso di così!
		# TODO: determinare un safeThreshold
		
		self.angData = deque()
		self.straightAngData = deque() # Dati relativi a tratti dritti di strada, con angoli diversi da 0
		self.nonStraightAngData = deque() # Dati relativi a tratti NON dritti di strada
		
		# Variabili che indicano se le costanti sono state trovate
		self.tanConstantsFound = False
		self.simpleAngConstantsFound = False
		self.complexAngConstantsFound = False

		# Angolo oltre il quale la macchina crasha
		self.crashAngle = 60 * math.pi / 180
	
	
	def seenSomeoneElse(self, car):
		"""
		Gli viene passata una macchina car **degli avversari** non crashata e senza altri vicini.
		car viene utilizzata soltanto per avere lo stato della macchina, non per chiamarci metodi.
		Non memorizza tutto, ma screma e tiene i dati importanti
		"""

		pass
		
	def seenMyself(self, car):
		"""
		Gli viene passata **la nostra macchina** car non crashata e senza altri vicini.
		car viene utilizzata soltanto per avere lo stato della macchina, non per chiamarci metodi.
		Non memorizza tutto, ma screma e tiene i dati importanti
		"""
		
		if not car.isTurboActive():
			# Analisi dell'accelerazione tangenziale
			
			self.tanData.append( TangentialDataPoint(throttle = car.throttle, velocity = car.previousVel, effTanAcc = car.acc) )
			
			# Se ci sono tanti valori, buttiamo via il più vecchio e tanti saluti
			if len(self.tanData) > 30:
				self.tanData.popleft()
			# if len(self.tanData) == 1000: pickle.dump(self.tanData, open("data.txt", "w"))
			
			# Fai regressione lineare sulle grandezze tangenziali (metodo dei minimi quadrati)
			if len(self.tanData) > 10:
				A = [[p.throttle, p.velocity] for p in self.tanData]
				B = [p.effTanAcc for p in self.tanData]
				
				res = lstsq(A, B)
				X = res[0]

				try:
					residual = res[1][0]
				except IndexError:
					residual = 99999999
				
				tanAccMax = X[0]
				tanVelMax = (-1/X[1])*X[0]
				
				# Sovrascrivo le costanti solo se il fit è molto buono
				if residual < 1e-20:
					self.tanAccMax = tanAccMax
					self.tanVelMax = tanVelMax

					# Cambiamo le costanti solo se sono nel "range di credibilità"
					if abs( self.tanAccMax - self.fixedTanAccMax ) < self.tanAccMaxVariation and abs( self.tanVelMax - self.fixedTanVelMax ) < self.tanVelMaxVariation:
						if not self.tanConstantsFound:
							print "*** Nuove costanti tangenziali: %lf, %lf ***" % (self.tanAccMax, self.tanVelMax)
						self.tanConstantsFound = True
					else:
						# print "*** Le costanti trovate non sono nell'intervallo di credibilità: %lf, %lf ***" % (self.tanAccMax, self.tanVelMax)
						pass

		# Analisi degli switch
		if self.tanConstantsFound and not car.isTurboActive():
			previousPosition = car.previousPosition

			if previousPosition \
			  and previousPosition.lane.startLaneIndex \
			  and previousPosition.lane.startLaneIndex != previousPosition.lane.endLaneIndex \
			  and previousPosition.pieceIndex != car.position.pieceIndex: # La macchina è appena uscita dallo switch

				realTanAcc = self.getTanAcc(car.previousVel, car.throttle, car.getTurboFactor())
				projectedPosition = previousPosition.inPieceDistance + car.previousVel + realTanAcc

				fakeStartPosition = Position(
					pieceIndex = (previousPosition.pieceIndex + 1) % len(car.gameData.track.pieces),
					inPieceDistance = 0.0,
					lane = car.position.lane,
					lap = previousPosition.lap + (((previousPosition.pieceIndex + 1) % len(car.gameData.track.pieces)) == 0)
				)
				overshoot = car.gameData.track.getDistance(fakeStartPosition, car.position)

				switchLength = projectedPosition - overshoot

				print "** Switch length =", switchLength

				previousPiece = car.gameData.track.getPiece(previousPosition.pieceIndex)
				if isinstance(previousPiece, StraightPiece):
					previousPiece.switchLength = switchLength
				elif isinstance(previousPiece, BendPiece):
					previousPiece.switchLength[(previousPosition.lane.startLaneIndex, previousPosition.lane.endLaneIndex)] = switchLength
					previousPiece.switchLength[(previousPosition.lane.endLaneIndex, previousPosition.lane.startLaneIndex)] = switchLength

	
	def seenSomebody(self, car):
		"""
		Gli viene passata una car non crashata e senza altri vicini, nostra o anche non nostra.
		car viene utilizzata soltanto per avere lo stato della macchina, non per chiamarci metodi.
		Non memorizza tutto, ma screma e tiene i dati importanti
		"""


		# Analisi dell'accelerazione angolare
		point = AngularDataPoint(curvature = car.previousCurvature, carAngle = car.previousAngle, velocity = car.previousVel, angularVelocity = car.previousAngVel, effAngAcc = car.angAcc)
		
		self.angData.append(point)
		if len(self.angData) > 400:
			self.angData.popleft()

		if abs(car.previousCurvature) <= 1e-10 and abs(car.previousAngle) > 1e-3:
			self.straightAngData.append(point)
			# Tengo solo 30 valori su cui fare il fit.
			if len(self.straightAngData) > 30:
				self.straightAngData.popleft()
		if abs(car.previousCurvature) >= 1e-5:
			self.nonStraightAngData.append(point)
			if len(self.nonStraightAngData) > 200:
				self.nonStraightAngData.popleft()
		
		if len(self.straightAngData) > 20:
			# Costanti angolari semplici
			A = [ [ p.angularVelocity, p.velocity * p.carAngle ] for p in self.straightAngData ]
			B = [ p.effAngAcc for p in self.straightAngData ]
			
			res = lstsq(A, B)
			X = res[0]

			# print res
			
			try:
				residual = res[1][0]
			except IndexError:
				residual = 99999999
			
			angVelConstant = X[0]
			unphysicalConstant = X[1]
			
			if residual < 1e-20:
				# TODO: fare un check di credibilità
				self.angVelConstant = angVelConstant
				self.unphysicalConstant = unphysicalConstant

				if not self.simpleAngConstantsFound:
					print "*** Nuove costanti angolari S: %lf, %lf ***" % (angVelConstant, unphysicalConstant)
				self.simpleAngConstantsFound = True
		
		if self.simpleAngConstantsFound:
			# Altre costanti angolari
			diffs = [ (p.effAngAcc - self.angVelConstant * p.angularVelocity - self.unphysicalConstant * p.velocity * p.carAngle) for p in self.angData ]
			# sortedAngData = sorted(self.angData, key = lambda x: x.velocity ** 2 * abs(x.curvature))
			
			# Prendo solo i dati che danno una differenza diversa da zero
			nonZeroAngData = [ p for p in self.nonStraightAngData if abs(p.effAngAcc - self.angVelConstant * p.angularVelocity - self.unphysicalConstant * p.velocity * p.carAngle) > 10e-10 ]
			
			# print len(nonZeroAngData)

			if len(nonZeroAngData) > 30:
				A = [ [ p.velocity ** 2 * math.sqrt( abs(p.curvature) ) * numpy.sign(p.curvature), p.velocity * numpy.sign(p.curvature) ] for p in nonZeroAngData ]
				B = [ p.effAngAcc - self.angVelConstant * p.angularVelocity - self.unphysicalConstant * p.velocity * p.carAngle for p in nonZeroAngData ]
			
				res = lstsq(A, B)
				X = res[0]
			
				try:
					residual = res[1][0]
				except IndexError:
					residual = 99999999
			
				vvkConstant = X[0]
				vConstant = X[1]
				
				if residual < 1e-20:
					# TODO: fare un check di credibilità
					self.vvkConstant = vvkConstant
					self.vConstant = vConstant
					if not self.complexAngConstantsFound:
						print "*** Nuove costanti angolari C: %lf, %lf ***" % (vvkConstant, vConstant)
					self.complexAngConstantsFound = True

	def getStaticFrictionTerm(self, velocity, curvature):
		v = velocity
		k = curvature
		return self.vvkConstant * v * v * math.sqrt( abs(k) ) + self.vConstant * v

	def getTanAcc(self, velocity, throttle, turbo=1.0):
		"""
		Restituisce l'accelerazione tangenziale stimata in base alla fisica conosciuta.
		Si assume che dipenda solo da velocity e da uno degli ultimi throttle, oltre che dal turbo.
		"""
		
		return self.tanAccMax * throttle * turbo - self.tanAccMax * velocity / self.tanVelMax
	
	def getAngAcc(self, curvature, carAngle, velocity, angularVelocity):
		"""
		Restituisce l'accelerazione angolare stimata in base alla fisica conosciuta.
		Si assume che dipenda solo da curvature, carAngle, velocity e angularVelocity.
		Si assume che gli angoli siano in radianti.
		"""
		
		k = curvature
		v = velocity

		if self.getStaticFrictionTerm(v, k) > 0.0:
			a = numpy.sign(k) * ( self.vvkConstant * v * v * math.sqrt( abs(k) ) + self.vConstant * v )
		else:
			a = 0.0

		return ( self.angVelConstant * angularVelocity + self.unphysicalConstant * velocity * carAngle + a )

	def getCrashAngle(self):
		# TODO: dedurre l'angolo soglia a partire da ciò che si osserva

		return self.crashAngle

	def getBrakingDistance(self, startVelocity, endVelocity):
		"""
		Restituisce la distanza minima necessaria per frenare da startVelocity a endVelocity.
		La frenata massima si assume che sia per throttle = 0.
		Restituisce 0 se startVelocity <= endVelocity.
		
		Si assume che la formula per getTanAcc() sia quella che abbiamo trovato, ovvero:
		self.tanAccMax * (throttle - velocity / self.tanVelMax )
		
		Non serve tenere conto del turbo, perché per throttle = 0 non ha alcuna influenza.
		"""
		
		if startVelocity == 0:
			return 0
		
		if startVelocity <= endVelocity:
			return 0
		
		# time = self.tanVelMax / self.tanAccMax * math.log( startVelocity / endVelocity ) # inutile
		distance = self.tanVelMax / self.tanAccMax * ( startVelocity - endVelocity )
		
		return distance

	def getBrakingTime(self, startVelocity, endVelocity):
		"""
		Restituisce il minimo numero (integer) di tick necessari per frenare da startVelocity a endVelocity
		(circa, perchè la simulazione poi è discreta).
		La frenata massima si assume che sia per throttle = 0.
		Restituisce 0 se startVelocity <= endVelocity.
		
		Si assume che la formula per getTanAcc() sia quella che abbiamo trovato, ovvero:
		self.tanAccMax * (throttle - velocity / self.tanVelMax )
		
		Non serve tenere conto del turbo, perché per throttle = 0 non ha alcuna influenza.
		"""

		if startVelocity == 0:
			return 0
		
		if startVelocity <= endVelocity:
			return 0
		
		time = self.tanVelMax / self.tanAccMax * math.log( startVelocity / endVelocity )
		
		return int(math.ceil(time))
		
	def getDriftVelocity(self, curvature, carAngle):
		"""
		Restituisce la velocità limite necessaria per mantenere l'angolo carAngle in una curva con curvatura curvature.
		carAngle è inteso verso l'esterno della curva, non deve essere considerato il segno di curvature e carAngle.
		Si assume che la formula per getAngAcc() sia quella che abbiamo trovato.
		"""
		
		curvature = abs(curvature)
		carAngle = abs(carAngle)

		if curvature == 0:
			# Warp speed :)
			return 9999999999
		
		# FIXME: per ora assumiamo che non ci sia attrito statico
		return - ( self.unphysicalConstant * carAngle + self.vConstant ) / ( self.vvkConstant * math.sqrt( curvature ) )


	def getThrottleForAcceleration(self, velocity, acceleration, turbo=1):
		"""
		Restituisce il throttle necessario per accelerare di acceleration
		Si assume che la formula per getTanAcc() sia quella che abbiamo trovato.
		"""
		
		throttle = ( acceleration / self.tanAccMax + velocity / self.tanVelMax ) / turbo
		# print "Desired throttle: %lf" % throttle

		if throttle < 0:
			throttle = 0
		if throttle > 1:
			throttle = 1
		
		# print "getThrottleForAcceleration. velocity = %lf, acceleration = %lf, throttle = %lf" % (velocity, acceleration, throttle)

		return throttle



