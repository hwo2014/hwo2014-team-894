#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from __future__ import division

import json
import sys
import traceback
import math
import os

from Car import *
from Track import *
from Piece import *
from GameData import *

class GameBot(object):

	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key
		
		self.gameData = None
		self.detective = None
		self.controlTower = None

	def sendMessage(self, msgType, data=None, gameTick=None, gameId=None):
		message = {
			"msgType": msgType
		}
		if data is not None:
			message["data"] = data
		if gameTick is not None:
			message["gameTick"] = gameTick
		if gameId is not None:
			message["gameId"] = gameId

		self.socket.sendall(json.dumps(message) + "\n")

	def handleMessage(self, msgType, data=None, gameTick=None, gameId=None):
		try:
			handler = getattr(self, "on_"+msgType)
		except AttributeError:
			handler = None

		if handler is not None:
			kwargs = {}
			if gameTick is not None:
				kwargs["gameTick"] = gameTick
			if gameId is not None:
				kwargs["gameId"] = gameId
			
			handler(data, **kwargs)
		else:
			print "Unknown message type: {0}. Data: {1}\n".format(msgType, data)
			self.ping()

	def receiveLoop(self):
		socket_file = self.socket.makefile()
		line = socket_file.readline()
		while line:
			try:
				message = json.loads(line)
				
				msgType = message['msgType']
				data = message['data']
				gameTick = None
				gameId = None
				if 'gameTick' in message:
					gameTick = message['gameTick']
				if 'gameId' in message:
					gameId = message['gameId']

				self.handleMessage(msgType, data, gameTick=gameTick, gameId=gameId)
			except Exception as err:
				print "Exception:", err
				print "The message was:", line
				print "Traceback:", traceback.format_exc()
				self.ping()
			finally:
				line = socket_file.readline()

		print "Connection closed."

	def run(self):
		try:
			if "IL_NOSTRO_TEST" in os.environ:
				# Nostro test
				self.joinRace(carCount=os.getenv("CARCOUNT", 1), trackName=os.getenv("TRACKNAME", "keimola"))
			else:
				# Gara
				self.join()
		except:
			# Boh, al riparo da eccezioni!
			self.join()
		# Loop
		self.receiveLoop()

	#####################
	#  Receive message  #
	#####################
	
	def on_join(self, data):
		print "join ack"

	def on_joinRace(self, data):
		print "joinRace ack"

	def on_createRace(self, data):
		print "createRace ack"

	def on_yourCar(self, data, gameId=None):
		print "yourCar"
		
		self.gameData.myCarId = CarId(
			data["name"],
			data["color"]
		)
		
		#self.ping()

	def on_gameInit(self, data, gameId=None):
		print "gameInit", data

		lanes = [None] * len(data["race"]["track"]["lanes"])
		for l in data["race"]["track"]["lanes"]:
			lanes[l["index"]] = Lane(
				distanceFromCenter = l["distanceFromCenter"],
				index = l["index"]
			)

		absoluteStartAngle = 0.0
		pieces = []
		for (index, p) in enumerate(data["race"]["track"]["pieces"]):
			if "radius" in p:
				pieces.append(BendPiece(
					lanes = lanes,
					absoluteStartAngle = absoluteStartAngle,
					radius = p["radius"],
					bendAngle = p["angle"]/180*math.pi,
					switch = "switch" in p and p["switch"]
				))

				absoluteStartAngle += p["angle"] / 180 * math.pi
			else:
				pieces.append(StraightPiece(
					lanes = lanes,
					absoluteStartAngle = absoluteStartAngle,
					length = p["length"],
					switch = "switch" in p and p["switch"]
				))

		self.gameData.track = Track(
			data["race"]["track"]["id"],
			data["race"]["track"]["name"],
			pieces,
			lanes
		)

		for car in data["race"]["cars"]:
			carId = CarId(
				name = car["id"]["name"],
				color = car["id"]["color"]
			)
			dimensions = Dimensions(
				length = car["dimensions"]["length"],
				width = car["dimensions"]["width"],
				guideFlagPosition = car["dimensions"]["guideFlagPosition"]
			)

			newCar = Car(
				self.gameData,
				carId,
				dimensions
			)

			if car["id"]["name"] in self.gameData.carsByName:
				oldCar = self.gameData.getCar(car["id"]["name"])
				newCar.crashCounter = oldCar.crashCounter

			self.gameData.addCar(newCar)

		if "quickRace" in data["race"]["raceSession"]:
			self.gameData.raceSession = RaceSession(
				laps = data["race"]["raceSession"]["laps"],
				maxLapTimeMs = data["race"]["raceSession"]["maxLapTimeMs"],
				quickRace = data["race"]["raceSession"]["quickRace"],
				durationMs = None
			)
			self.gameData.track.setLaps(data["race"]["raceSession"]["laps"])
		else:
			self.gameData.raceSession = RaceSession(
				laps = None,
				maxLapTimeMs = None,
				quickRace = None,
				durationMs = data["race"]["raceSession"]["durationMs"]
			)
		
		self.controlTower.gameInit()

		#self.ping()

	def on_carPositions(self, data, gameTick=None, gameId=None):
		#print "carPositions", data, gameTick

		for carData in data:
			lanePosition = LanePosition(
				startLaneIndex = carData["piecePosition"]["lane"]["startLaneIndex"],
				endLaneIndex = carData["piecePosition"]["lane"]["endLaneIndex"]
			)

			position = Position(
				pieceIndex = carData["piecePosition"]["pieceIndex"],
				inPieceDistance = carData["piecePosition"]["inPieceDistance"],
				lane = lanePosition,
				lap = carData["piecePosition"]["lap"]
			)

			car = self.gameData.getCarByName(carData["id"]["name"])
			
			car.updatePosition(
				carData["angle"]/180*math.pi,
				position,
				gameTick
			)

			# Quando la macchina crasha ci dà dati che sono tutti sbagliati
			# Quando ci sono urti i dati sono tutti sbagliati
			if not car.crashed \
				and not car.disqualified \
				and len(self.gameData.getCarsNear(car, 20)) == 0:
				
				# Manda gli indizi al detective
				if self.gameData.isMyCar(car):
					self.detective.seenMyself(car)
				else:
					self.detective.seenSomeoneElse(car)

				self.detective.seenSomebody(car)

		car = self.gameData.getMyCar()
		turbo = car.getTurboFactor()
		
		#print ""

		expectedTanAcc = self.controlTower.simulator.detective.getTanAcc(car.previousVel, car.throttle, car.getTurboFactor())
		tanError = car.acc - expectedTanAcc
		print "Expected tangential acceleration: %lf (errore assoluto: %lf)" % (expectedTanAcc, tanError)
	
		expectedAngAcc = self.controlTower.simulator.detective.getAngAcc(car.previousCurvature, car.previousAngle, car.previousVel, car.previousAngVel)
		angError = car.angAcc - expectedAngAcc
		print "Expected angular acceleration: %lf (errore assoluto: %lf)" % (expectedAngAcc, angError)
	
		car.printStatus()
		
		
		if gameTick != None:
			self.controlTower.checkMode()
			
			action = self.controlTower.getAction(tick = gameTick)
			
			if isinstance(action, ThrottleAction):
				self.throttle(action.throttle, action.tick)
			elif isinstance(action, SwitchAction):
				self.switchLane(action.switch, action.tick)
			elif isinstance(action, TurboAction):
				self.turbo(action.message, action.tick)
			else:
				self.ping()

	def on_gameStart(self, data, gameTick=None, gameId=None):
		print "gameStart"
		
		if gameTick != None:
			self.controlTower.checkMode()
			
			action = self.controlTower.getAction(tick = gameTick)
			
			if isinstance(action, ThrottleAction):
				assert self.gameData.getMyCar().throttle == action.throttle
				self.throttle(action.throttle, action.tick)
			elif isinstance(action, SwitchAction):
				self.switchLane(action.switch, action.tick)
			elif isinstance(action, TurboAction):
				self.turbo(action.message, action.tick)
			else:
				self.ping()

	def on_gameEnd(self, data, gameId=None):
		print "gameEnd", data

		# TODO

		self.ping()

	def on_tournamentEnd(self, data, gameId=None):
		print "tournamentEnd"
		self.ping()

	def on_crash(self, data, gameTick=None, gameId=None):
		print "crash", data, gameTick

		car = self.gameData.getCarByName(data["name"])
		car.setCrashed(True, gameTick);

		#self.ping()

	def on_spawn(self, data, gameTick=None, gameId=None):
		print "spawn", data, gameTick

		car = self.gameData.getCarByName(data["name"])
		car.setCrashed(False, gameTick);

		#self.ping()

	def on_lapFinished(self, data, gameTick=None, gameId=None):
		print "lapFinished", data, gameTick

		# TODO

		#self.ping()

	def on_dnf(self, data, gameTick=None, gameId=None):
		print "dnf", data, gameTick

		car = self.gameData.getCarByName(data["name"])
		car.setDisqualified(data["reason"], gameTick);

		#self.ping()

	def on_finish(self, data, gameTick=None, gameId=None):
		print "finish", data, gameTick
		self.ping()

	def on_turboAvailable(self, data, gameTick=None, gameId=None):
		print "turboAvailable", data, gameTick
		
		turbo = Turbo(
			turboDurationMilliseconds = data["turboDurationMilliseconds"],
			turboDurationTicks = data["turboDurationTicks"],
			turboFactor = data["turboFactor"]
		)

		for car in self.gameData.getCars():
			car.setTurboAvailable(turbo)
		
		#self.ping()

	def on_turboStart(self, data, gameTick=None, gameId=None):
		print "turboStart", data, gameTick
		
		car = self.gameData.getCarByName(data["name"])
		car.turboStart(gameTick)
		
		#self.ping()

	def on_turboEnd(self, data, gameTick=None, gameId=None):
		print "turboEnd", data, gameTick
		
		car = self.gameData.getCarByName(data["name"])
		car.turboEnd(gameTick)
		
		#self.ping()

	##################
	#  Send message  #
	##################
	
	def turbo(self, message, gameTick):
		print "Tick {0}: turbo {1}".format(gameTick, message)
		self.sendMessage("turbo", message, gameTick=gameTick)

	def throttle(self, throttle, gameTick):
		#print "Tick {0}: throttle {1}".format(gameTick, throttle)
		self.sendMessage("throttle", throttle, gameTick=gameTick)

	def switchLane(self, direction, gameTick):
		print "Tick {0}: switchLane {1}".format(gameTick, direction)
		
		if direction in ["Left", "Right"]:
			self.sendMessage("switchLane", direction, gameTick=gameTick)
		else:
			print "/!\\Error: switchLane not valid: ", direction
			self.ping()

	def join(self):
		print "join"
		data = {
			"name": self.name,
			"key": self.key
		}
		self.sendMessage("join", data)
	
	def createRace(self, carCount=3, trackName=None, password=None):
		print "createRace", carCount, trackName, password
		data = {
			"botId": {
				"name": self.name,
				"key": self.key
			},
			"carCount": carCount
		}
		if password is not None:
			data["password"] = password
		if trackName is not None:
			data["trackName"] = trackName
		
		self.sendMessage("createRace", data)

	def joinRace(self, carCount=None, trackName=None, password=None):
		print "joinRace", carCount, trackName, password
		data = {
			"botId": {
				"name": self.name,
				"key": self.key
			}
		}
		if carCount is not None:
			data["carCount"] = carCount
		if password is not None:
			data["password"] = password
		if trackName is not None:
			data["trackName"] = trackName

		self.sendMessage("joinRace", data)
	
	def ping(self):
		print "ping"
		self.sendMessage("ping")
