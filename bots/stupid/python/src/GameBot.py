#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# pylint: disable=W191

import json
import sys
import os
import random

class GameBot(object):

	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key

	def sendMessage(self, msgType, data=None, gameTick=None, gameId=None):
		message = {
			"msgType": msgType
		}
		if data is not None:
			message["data"] = data
		if gameTick is not None:
			message["gameTick"] = gameTick
		if gameId is not None:
			message["gameId"] = gameId
		self.socket.sendall(json.dumps(message) + "\n")

	def handleMessage(self, msgType, data=None, gameTick=None, gameId=None):
		handler = getattr(self, "on_"+msgType)

		if handler is not None:
			kwargs = {}
			if gameTick is not None:
				kwargs["gameTick"] = gameTick
			if gameId is not None:
				kwargs["gameId"] = gameId
			
			handler(data, **kwargs)
		else:
			print "Unhandled message: {0}\n".format(msgType)
			self.ping()

	def receiveLoop(self):
		socket_file = self.socket.makefile()
		line = socket_file.readline()
		while line:
			try:
				message = json.loads(line)

				msgType = message['msgType']
				data = message['data']
				gameTick = None
				gameId = None
				if 'gameTick' in message:
					gameTick = message['gameTick']
				if 'gameId' in message:
					gameId = message['gameId']

				self.handleMessage(msgType, data, gameTick=gameTick, gameId=gameId)
			except Exception as e:
				print "Exception", e
				print "The message was", line
				self.ping()
			finally:
				line = socket_file.readline()

		print "Connection closed."

	def run(self):
		try:
			if "IL_NOSTRO_TEST" in os.environ:
				# Nostro test
				self.joinRace(carCount=os.getenv("CARCOUNT", 1), trackName=os.getenv("TRACKNAME", "keimola"))
			else:
				# Gara
				self.join()
		except:
			# Boh, al riparo da eccezioni!
			self.join()
		# Loop
		self.receiveLoop()

	#####################
	#  Receive message  #
	#####################
	
	def on_join(self, data):
		print "join ack"

	def on_joinRace(self, data):
		print "joinRace ack"

	def on_createRace(self, data):
		print "createRace ack"

	def on_yourCar(self, data, gameId=None):
		print "yourCar"

	def on_gameInit(self, data, gameId=None):
		print "gameInit"

	def on_carPositions(self, data, gameTick=None, gameId=None):
		if gameTick < 5 or gameTick % 51 == 0:
			print "carPositions", data
		if gameTick % 23 == 0:
			switch = random.choice(["Left", "Right", None])
			if switch is not None:
				self.switchLane(switch, gameTick)
			else:
				self.throttle(0.5, gameTick)
		else:
			self.throttle(random.uniform(0.5, 0.7), gameTick)

	def on_gameStart(self, data, gameTick=None, gameId=None):
		print "gameStart"

	def on_gameEnd(self, data, gameId=None):
		print "gameEnd", data

	def on_tournamentEnd(self, data, gameId=None):
		print "tournamentEnd"
		self.ping()

	def on_crash(self, data, gameTick=None, gameId=None):
		print "crash", data, gameTick

	def on_spawn(self, data, gameTick=None, gameId=None):
		print "spawn", data, gameTick

	def on_lapFinished(self, data, gameTick=None, gameId=None):
		print "lapFinished", data, gameTick

	def on_dnf(self, data, gameTick=None, gameId=None):
		print "dnf", data, gameTick

	def on_finish(self, data, gameTick=None, gameId=None):
		print "finish", data, gameTick
		self.ping()

	def on_turboAvailable(self, data, gameTick=None, gameId=None):
		print "turboAvailable", data, gameTick

	def on_turboStart(self, data, gameTick=None, gameId=None):
		print "turboStart", data, gameTick

	def on_turboEnd(self, data, gameTick=None, gameId=None):
		print "turboEnd", data, gameTick

	##################
	#  Send message  #
	##################
	
	def turbo(self, message, gameTick):
		print "Tick {0}: turbo {1}".format(gameTick, message)
		self.sendMessage("turbo", message, gameTick=gameTick)

	def throttle(self, throttle, gameTick):
		#print "Tick {0}: throttle {1}".format(gameTick, throttle)
		self.sendMessage("throttle", throttle, gameTick=gameTick)

	def switchLane(self, direction, gameTick):
		print "Tick {0}: switchLane {1}".format(gameTick, direction)
		
		if direction in ["Left", "Right"]:
			self.sendMessage("switchLane", direction, gameTick=gameTick)
		else:
			print "/!\\Error: switchLane not valid: ", direction
			self.ping()

	def join(self):
		print "join"
		data = {
			"name": self.name,
			"key": self.key
		}
		self.sendMessage("join", data)
	
	def createRace(self, carCount=3, trackName=None, password=None):
		print "createRace", carCount, trackName, password
		data = {
			"botId": {
				"name": self.name,
				"key": self.key
			},
			"carCount": int(carCount)
		}
		if password is not None:
			data["password"] = password
		if trackName is not None:
			data["trackName"] = trackName
		
		self.sendMessage("createRace", data)

	def joinRace(self, carCount=None, trackName=None, password=None):
		print "joinRace", carCount, trackName, password
		data = {
			"botId": {
				"name": self.name,
				"key": self.key
			}
		}
		if carCount is not None:
			data["carCount"] = int(carCount)
		if password is not None:
			data["password"] = password
		if trackName is not None:
			data["trackName"] = trackName

		self.sendMessage("joinRace", data)
	
	def ping(self):
		print "ping"
		self.sendMessage("ping")
